package com.ust.trekker.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import uk.co.senab.photoview.IPhotoView;

public class AnimationFactory {

    public static void crossFade(final View v1, final View v2, final int duration) {
        v2.setAlpha(0.0f);
        v2.setVisibility(View.VISIBLE);
        v1.animate().alpha(0.0f).setDuration((long) duration).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                v1.setVisibility(View.GONE);
                v2.animate().alpha(IPhotoView.DEFAULT_MIN_SCALE).setDuration((long) duration).setListener(null);
            }
        });
    }

    public static void fadeIn(View v, int duration) {
        v.setAlpha(0.0f);
        v.animate().alpha(IPhotoView.DEFAULT_MIN_SCALE).setDuration((long) duration).setListener(null);
    }

    public static void fadeOut(View v, int duration) {
        v.setAlpha(IPhotoView.DEFAULT_MIN_SCALE);
        v.animate().alpha(0.0f).setDuration((long) duration).setListener(null);
    }

    public static void move(View v) {
    }
}
