package com.ust.trekker.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.ust.trekker.R;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Article;
import com.ust.trekker.model.EssayQuestion;
import com.ust.trekker.model.GameZoneQuestion;
import com.ust.trekker.model.MCQuestion;
import java.text.SimpleDateFormat;
import java.util.Date;
import uk.co.senab.photoview.BuildConfig;
import uk.co.senab.photoview.IPhotoView;

public class LayoutFactory {

    public static class SimpleLayout {
        Article article;
        TextView descView;
        boolean isThumbnail;
        View layout;
        ImageView photoView;
        TextView titleView;

        public SimpleLayout(View layout, Article article, boolean thumbnail) {
            this.article = article;
            this.layout = layout;
            this.titleView = (TextView) layout.findViewById(R.id.simple_title);
            this.descView = (TextView) layout.findViewById(R.id.simple_desc);
            this.photoView = (ImageView) layout.findViewById(R.id.simple_image);
            this.isThumbnail = thumbnail;
            setTitle(article.getTitle());
            setDesc(article.getContent());
            setImage(article.getCoverContent());
        }

        public void setTitle(String title) {
            if (this.titleView != null) {
                this.titleView.setText(title);
            }
        }

        public void setDesc(String desc) {
            if (this.descView != null) {
                this.descView.setText(desc);
            }
        }

        public void setImage(Bitmap bitmap) {
            if (this.photoView != null) {
                if (this.isThumbnail) {
                    bitmap = ThumbnailUtils.extractThumbnail(bitmap, IPhotoView.DEFAULT_ZOOM_DURATION, IPhotoView.DEFAULT_ZOOM_DURATION);
                }
                this.photoView.setImageBitmap(bitmap);
            }
        }

        public void setImage(String base64Image) {
            if (this.photoView != null) {
                setImage(Util.base64ToBitmap(base64Image));
            }
        }

        public TextView getTitleView() {
            return this.titleView;
        }

        public TextView getDescView() {
            return this.descView;
        }

        public ImageView getPhotoView() {
            return this.photoView;
        }

        public View getLayout() {
            return this.layout;
        }
    }

    public static class DateSimpleLayout extends SimpleLayout {
        String date;
        TextView dateView;

        public DateSimpleLayout(View layout, Article article) {
            super(layout, article, true);
            this.date = new SimpleDateFormat(App.SQLITE_DATE_FORMAT).format(new Date());
            this.dateView = (TextView) layout.findViewById(R.id.simple_date);
        }

        public TextView getDateView() {
            return this.dateView;
        }
    }

    public static SimpleLayout getSimpleLayout(View view, Article article, boolean thumbnail) {
        return new SimpleLayout(view, article, thumbnail);
    }

    public static DateSimpleLayout getDateSimpleLayout(Context context, Article article) {
        return getDateSimpleLayout(((LayoutInflater) context.getSystemService("layout_inflater")).inflate(R.layout.simple_date_list_style_paper_bg, null), article);
    }

    public static DateSimpleLayout getDateSimpleLayout(View view, Article article) {
        return new DateSimpleLayout(view, article);
    }

    public static View getQuestionLayout(Context context, ViewGroup container, int index, GameZoneQuestion question) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService("layout_inflater");
        View view;
        TextView questionView;
        if (question instanceof MCQuestion) {
            view = inflater.inflate(R.layout.game_zone_mc_question_item, container);
            questionView = (TextView) view.findViewById(R.id.question);
            RadioGroup mcAns = (RadioGroup) view.findViewById(R.id.mc_answers);
            ((TextView) view.findViewById(R.id.id)).setText(index + BuildConfig.FLAVOR);
            questionView.setText(question.getQuestion());
            for (String answer : ((MCQuestion) question).getAnswers()) {
                RadioButton radioButton = new RadioButton(context);
                radioButton.setText(answer);
                mcAns.addView(radioButton);
            }
            return view;
        } else if (!(question instanceof EssayQuestion)) {
            return null;
        } else {
            view = inflater.inflate(R.layout.game_zone_essay_question_item, container);
            questionView = (TextView) view.findViewById(R.id.question);
            ((TextView) view.findViewById(R.id.id)).setText(index + BuildConfig.FLAVOR);
            questionView.setText(question.getQuestion());
            return view;
        }
    }

    public static View getSimpleArticle1(Context context, Article simpleArticle) {
        View root = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.simple_article_layout, null);
        TextView dateView = (TextView) root.findViewById(R.id.simple_article_date);
        TextView contentView = (TextView) root.findViewById(R.id.simple_article_content);
        ((TextView) root.findViewById(R.id.simple_article_title)).setText(simpleArticle.getTitle());
        dateView.setText(simpleArticle.getCreateDate());
        contentView.setText(simpleArticle.getContent());
        return root;
    }
}
