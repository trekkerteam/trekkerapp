package com.ust.trekker.util;

import com.ust.trekker.fragment.CustomFragment;
import java.util.Stack;

public class FragmentStack extends Stack<CustomFragment> {
    private static FragmentStack ourInstance;

    static {
        ourInstance = new FragmentStack();
    }

    public static FragmentStack getInstance() {
        return ourInstance;
    }

    private FragmentStack() {
    }

    public void switchMainPage(CustomFragment fragment) {
        while (size() > 1) {
            pop();
        }
        add(fragment);
    }

    public void backToHome() {
        while (size() > 1) {
            pop();
        }
    }
}
