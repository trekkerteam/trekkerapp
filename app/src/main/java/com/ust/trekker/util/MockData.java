package com.ust.trekker.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.ust.trekker.R;
import com.ust.trekker.model.EssayQuestion;
import com.ust.trekker.model.GameZoneQuestion;
import com.ust.trekker.model.MCQuestion;
import com.ust.trekker.model.NewsLetter;
import com.ust.trekker.model.SimpleArticle;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MockData {
    public static List<NewsLetter> getMockNewsLetter() {
        List<NewsLetter> newsLetters = new ArrayList();
        for (int i = 0; i < 5; i++) {
            NewsLetter newsLetter = new NewsLetter();
            newsLetter.setDate(new Date());
            newsLetters.add(newsLetter);
        }
        return newsLetters;
    }

    public static List<GameZoneQuestion> getQuestion() {
        List<GameZoneQuestion> questions = new ArrayList();
        questions.add(new EssayQuestion("I don't want to use the back stack. Is there a way to do this?"));
        questions.add(new EssayQuestion("I don't want to use the back stack. Is there a way to do this?"));
        questions.add(new EssayQuestion("I don't want to use the back stack. Is there a way to do this?"));
        for (int i = 0; i < 3; i++) {
            questions.add(new MCQuestion("This is an MC question: I don't want to use the back stack. Is there a way to do this?", new String[]{"answer A", "answer B", "answer C"}));
        }
        return questions;
    }

    public static SimpleArticle getSimpleArticle(Context context) {
        SimpleArticle simpleArticle = new SimpleArticle();
        simpleArticle.setTitle("India: Kolkata flyover collapses 'killing at least 10'");
        simpleArticle.setAuthor("Kolkata");
        simpleArticle.setDate(new Date());
        simpleArticle.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.demo_photo6));
        simpleArticle.setContent("Reports suggest as many as 150 could be trapped under the concrete and steel bridge, which fell on a busy road.\nImages showed residents using their bare hands to help the rescue effort.\nConstruction projects in India have often suffered from safety issues with frequent collapses.\nExperts say there is a lack of inspections and substandard materials are used.\nThe flyover has been under construction since 2009 and has missed several deadlines for completion, Reuters news agency said.\nThe accident took place in an area near Girish Park, one of Kolkata's most densely populated neighbourhoods, with narrow lanes, and shops and houses built close together.");
        return simpleArticle;
    }

    public static List<Bitmap> getPhotos(Context context) {
        List<Bitmap> bitmaps = new ArrayList();
        for (int i = 0; i < 5; i++) {
            bitmaps.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.demo_photo6));
        }
        return bitmaps;
    }
}
