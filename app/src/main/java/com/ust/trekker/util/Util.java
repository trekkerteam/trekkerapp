package com.ust.trekker.util;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.ust.trekker.constant.App;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Pattern;
import uk.co.senab.photoview.BuildConfig;

public class Util {
    private static final String TAG = "util";

    public static Bitmap base64ToBitmap(String base64String) {
        if (TextUtils.isEmpty(base64String)) {
            return null;
        }
        byte[] decodedString = Base64.decode(extractPhotoString(base64String), 0);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public static File binaryToFile(Context contexts, byte[] byteArray, String filename) throws IOException {
        try {
            File dir = new File(contexts.getFilesDir(), App.FILE_DIRECTORY);
            if (!(dir.exists() || dir.mkdir())) {
                Log.e(TAG, "fail create directory");
            }
            File file = new File(dir, filename.replace(" ", "_"));
            FileOutputStream fos = new FileOutputStream(file.getPath());
            fos.write(byteArray);
            fos.close();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "file saving failed.");
            return null;
        }
    }

    public static String extractPhotoString(String base64Str) {
        if (base64Str == null || !base64Str.contains(";")) {
            return base64Str;
        }
        String[] tmp = base64Str.split(";");
        return tmp[1].contains(",") ? tmp[1].split(",")[1] : tmp[1];
    }

//    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
//        for (RunningServiceInfo service : ((ActivityManager) context.getSystemService("activity")).getRunningServices(ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED)) {
//            if (serviceClass.getName().equals(service.service.getClassName())) {
//                return true;
//            }
//        }
//        return false;
//    }

    public static String thisYear() {
        return String.valueOf(Calendar.getInstance().get(1));
    }

    public static boolean haveNetwork(Context context) {
        try {
            NetworkInfo netInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            if (netInfo == null || !netInfo.isConnectedOrConnecting()) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String convertDate(String srcFormat, String dstFormat, String dateStr) throws ParseException {
        return new SimpleDateFormat(dstFormat, Locale.US).format(new SimpleDateFormat(srcFormat, Locale.US).parse(dateStr));
    }

    public static String getSateFormatFromStr(String dateStr) {
        if (Pattern.compile("\\d{1,2}\\/\\d{1,2}\\/\\d{4} \\d{1,2}:\\d{1,2}:\\d{1,2} [AP]M").matcher(dateStr).matches()) {
            return "MM/dd/yyyy H:mm:ss a";
        }
        if (Pattern.compile("\\d{4}\\/\\d{1,2}\\/\\d{1,2} \\d:\\d{1,2}:\\d{1,2}$").matcher(dateStr).matches()) {
            return "yyyy/MM/dd H:mm:ss";
        }
        return "";
    }

    public static String getDateFormat(String dateStr) {
        int i;
        String[] dateStrs = dateStr.split(" ")[0].split("/");
        String dateFormat = BuildConfig.FLAVOR;
        int l = dateStrs[0].length();
        for (i = 0; i < l; i++) {
            dateFormat = dateFormat + "y";
        }
        dateFormat = dateFormat + "/";
        l = dateStrs[1].length();
        for (i = 0; i < l; i++) {
            dateFormat = dateFormat + "M";
        }
        dateFormat = dateFormat + "/";
        l = dateStrs[2].length();
        for (i = 0; i < l; i++) {
            dateFormat = dateFormat + "d";
        }
        return dateFormat;
    }
}
