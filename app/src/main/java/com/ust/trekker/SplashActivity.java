package com.ust.trekker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
//import com.google.android.gms.common.GoogleApiAvailability;
import com.ust.trekker.Dao.ArticleDao;
import com.ust.trekker.Dao.CategoryDao;
import com.ust.trekker.constant.App;
import com.ust.trekker.db.DBHelper;
import com.ust.trekker.net.DownloadDataHelper;
import com.ust.trekker.net.DownloadDataHelper.Callback;

public class SplashActivity extends AppCompatActivity {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String TAG = "splashActivity";
    private ArticleDao articleDao;
    private CategoryDao categoryDao;
    private boolean isReceiverRegistered;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if (getSupportActionBar() != null) {
            int color;
            if (App.ENV.equals(App.DEV_LOGIN_TOKEN)) {
                color = getResources().getColor(R.color.dev_action_bar);
            } else if (App.ENV.equals(App.UAT_LOGIN_TOKEN)) {
                color = getResources().getColor(R.color.uat_action_bar);
            } else {
                color = getResources().getColor(R.color.pro_action_bar);
            }
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
        }
        this.categoryDao = CategoryDao.getInstance(this);
        this.articleDao = ArticleDao.getInstance(this);
        this.mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean sentToken = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(App.SENT_TOKEN_TO_SERVER, false);
            }
        };
        registerReceiver();

        start();
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(this.mRegistrationBroadcastReceiver);
        this.isReceiverRegistered = false;
        super.onPause();
    }

    public void start() {
        App.login = true;
        updateData();
    }

    private void updateData() {
        DownloadDataHelper.updateCategory(this.categoryDao, new Callback() {
            @Override
            public void onDownloadFinished() {
                DownloadDataHelper.updateArticle(SplashActivity.this.articleDao, new Callback() {
                    public void onDownloadFinished() {
                        SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        SplashActivity.this.finish();
                    }
                });
            }
        });
    }

    private void registerReceiver() {
        if (!this.isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(this.mRegistrationBroadcastReceiver, new IntentFilter(App.REGISTRATION_COMPLETE));
            this.isReceiverRegistered = true;
        }
    }

//    private boolean checkPlayServices() {
//        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
//        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
//        if (resultCode == 0) {
//            return true;
//        }
//        if (apiAvailability.isUserResolvableError(resultCode)) {
//            apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
//        } else {
//            Log.i(TAG, "This device is not supported.");
//            finish();
//        }
//        return false;
//    }

    public void loadDate() {
    }

    public void clearDatabase() {
        DBHelper helper = new DBHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.rawQuery("DELETE FROM Category", new String[0]);
        db.rawQuery("DELETE FROM Article", new String[0]);
        helper.close();
        db.close();
    }
}
