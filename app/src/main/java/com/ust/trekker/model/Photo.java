package com.ust.trekker.model;

public class Photo {
    private int ArticleId;
    public String Content;
    private int Id;
    private String Name;
    public int albumId;

    public int getAlbumId() {
        return this.albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public String getContent() {
        return this.Content;
    }

    public void setContent(String content) {
        this.Content = content;
    }

    public String getName() {
        return this.Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public int getArticleId() {
        return this.ArticleId;
    }

    public void setArticleId(int articleId) {
        this.ArticleId = articleId;
    }

    public int getId() {
        return this.Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public void clone(Photo photo) {
        this.Id = photo.Id;
        this.albumId = photo.albumId;
        this.Content = photo.Content;
        this.Name = photo.Name;
        this.ArticleId = photo.ArticleId;
    }
}
