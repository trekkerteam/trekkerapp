package com.ust.trekker.model;

import java.util.ArrayList;
import java.util.List;

public abstract class GameZoneQuestion extends Article {
    protected String answer;
    Article article;
    protected String question;
    List<Question> questions;

    public GameZoneQuestion(String question) {
        this.questions = new ArrayList();
        this.question = question;
    }

    public String getQuestion() {
        return this.question;
    }

    public String getAnswer() {
        return this.answer;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
