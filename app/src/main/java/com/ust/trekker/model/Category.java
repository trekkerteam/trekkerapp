package com.ust.trekker.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import com.ust.trekker.util.Util;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Category implements Comparable<Category> {
    private String CategorName;
    private String CreateDate;
    private int Id;
    private int ParentCatId;

    public int getId() {
        return this.Id;
    }

    public int getParentCatId() {
        return this.ParentCatId;
    }

    public String getCategorName() {
        return this.CategorName;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public void setParentCatId(int parentCatId) {
        this.ParentCatId = parentCatId;
    }

    public void setCategorName(String categorName) {
        this.CategorName = categorName;
    }

    public String getCreateDate() {
        return this.CreateDate;
    }

    public void setCreateDate(String createDate) {
        this.CreateDate = createDate;
    }

    public static List<String> getAllCateName(List<Category> categories) {
        if (categories == null) {
            return new ArrayList();
        }
        List<String> catNames = new ArrayList();
        for (Category c : categories) {
            catNames.add(c.getCategorName());
        }
        return catNames;
    }

    public void save(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(com.ust.trekker.db.DBContract.Category.COLUMN_NAME_PARENT_CAT_ID, Integer.valueOf(this.ParentCatId));
        values.put(com.ust.trekker.db.DBContract.Category.COLUMN_NAME_CAT_NAME, this.CategorName);
        values.put(com.ust.trekker.db.DBContract.Category.COLUMN_NAME_CAT_ID, Integer.valueOf(this.Id));
        db.insert(com.ust.trekker.db.DBContract.Category.TABLE_NAME, null, values);
    }

    public static Category[] load(SQLiteDatabase db) {
        List<Category> categories = new ArrayList();
        Cursor c = db.rawQuery("SELECT * FROM Category", new String[0]);
        while (c.moveToNext()) {
            Category category = new Category();
            category.CategorName = c.getString(c.getColumnIndexOrThrow(com.ust.trekker.db.DBContract.Category.COLUMN_NAME_CAT_NAME));
            category.ParentCatId = c.getInt(c.getColumnIndexOrThrow(com.ust.trekker.db.DBContract.Category.COLUMN_NAME_PARENT_CAT_ID));
            category.Id = c.getInt(c.getColumnIndexOrThrow(com.ust.trekker.db.DBContract.Category.COLUMN_NAME_CAT_ID));
            categories.add(category);
        }
        return (Category[]) categories.toArray(new Category[categories.size()]);
    }

    public int compareTo(@NonNull Category another) {
        try {
            DateFormat df = new SimpleDateFormat(Util.getSateFormatFromStr(another.getCreateDate()), Locale.ENGLISH);
            return df.parse(getCreateDate()).compareTo(df.parse(another.getCreateDate())) * -1;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
