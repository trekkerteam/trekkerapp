package com.ust.trekker.model;

import android.text.TextUtils;
import uk.co.senab.photoview.BuildConfig;

public class Article {
    String Author;
    int CategoryId;
    String Content;
    String CreateDate;
    int Id;
    String Title;
    String coverContent;
    String newsDate;
    String newsLink;
    String newsSource;
    String photoIds;

    public int getId() {
        return this.Id;
    }

    public String getTitle() {
        return this.Title;
    }

    public String getAuthor() {
        return this.Author;
    }

    public String getContent() {
        if (TextUtils.isEmpty(this.Content)) {
            this.Content = BuildConfig.FLAVOR;
        }
        return this.Content;
    }

    public String getCreateDate() {
        return this.CreateDate;
    }

    public int getCategoryId() {
        return this.CategoryId;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public void setTitle(String title) {
        this.Title = title;
    }

    public void setAuthor(String author) {
        this.Author = author;
    }

    public void setContent(String content) {
        this.Content = content;
    }

    public void setCreateDate(String createDate) {
        this.CreateDate = createDate;
    }

    public void setCategoryId(int categoryId) {
        this.CategoryId = categoryId;
    }

    public String getCoverContent() {
        return this.coverContent;
    }

    public void setCoverContent(String coverContent) {
        this.coverContent = coverContent;
    }

    public String getNewsSource() {
        return this.newsSource;
    }

    public void setNewsSource(String newsSource) {
        this.newsSource = newsSource;
    }

    public String getNewsDate() {
        return this.newsDate;
    }

    public String getNewsLink() {
        return this.newsLink;
    }

    public void setNewsDate(String newsDate) {
        this.newsDate = newsDate;
    }

    public void setNewsLink(String newsLink) {
        this.newsLink = newsLink;
    }

    public String getPhotoIds() {
        return this.photoIds;
    }

    public void setPhotoIds(String photoIds) {
        this.photoIds = photoIds;
    }

    public void copy(Article article) {
        this.Id = article.getId();
        this.Title = article.getTitle();
        this.Author = article.getAuthor();
        this.Content = article.getContent();
        this.CreateDate = article.getCreateDate();
        this.coverContent = article.getCoverContent();
        this.CategoryId = article.getCategoryId();
        this.photoIds = article.getPhotoIds();
    }
}
