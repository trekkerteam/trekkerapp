package com.ust.trekker.model;

import android.graphics.Bitmap;
import java.util.Date;

public class SimpleArticle {
    private String author;
    private String content;
    private Date date;
    private Bitmap image;
    private String title;

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getDate() {
        return this.date;
    }

    public String getTitle() {
        return this.title;
    }

    public String getContent() {
        return this.content;
    }

    public Bitmap getImage() {
        return this.image;
    }

    public String getAuthor() {
        return this.author;
    }
}
