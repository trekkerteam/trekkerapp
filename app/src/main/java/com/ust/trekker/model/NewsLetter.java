package com.ust.trekker.model;

import android.graphics.Bitmap;
import java.util.Date;

public class NewsLetter extends Article {
    private String content;
    private Date date;
    private Bitmap image;
    private String title;

    public long getDateTimestamp() {
        return this.date.getTime();
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
