package com.ust.trekker.model;

public class MCQuestion extends GameZoneQuestion {
    String[] answers;
    int numAnswers;

    public MCQuestion(String question, String[] answers) {
        super(question);
        this.answers = answers;
        this.numAnswers = answers.length;
    }

    public int getNumAnswers() {
        return this.numAnswers;
    }

    public void setNumAnswers(int numAnswers) {
        this.numAnswers = numAnswers;
    }

    public String[] getAnswers() {
        return this.answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }
}
