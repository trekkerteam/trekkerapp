package com.ust.trekker.model;

public class PhotoAlbum {
    int Cover;
    String CoverContent;
    int Id;
    String Name;
    String createDate;
    String photoIds;

    public String getName() {
        return this.Name;
    }

    public int getCover() {
        return this.Cover;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public void setCover(int cover) {
        this.Cover = cover;
    }

    public int getId() {
        return this.Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getCoverContent() {
        return this.CoverContent;
    }

    public void setCoverContent(String coverContent) {
        this.CoverContent = coverContent;
    }

    public String getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getPhotoIds() {
        return this.photoIds;
    }

    public void setPhotoIds(String photoIds) {
        this.photoIds = photoIds;
    }
}
