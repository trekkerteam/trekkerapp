package com.ust.trekker.model;

public class Response {
    public String ErrCode;
    public String ErrMsg;
    public String ResponseBody;

    public String getErrMsg() {
        return this.ErrMsg;
    }

    public String getErrCode() {
        return this.ErrCode;
    }

    public String getResponseBody() {
        return this.ResponseBody;
    }

    public void setErrMsg(String errMsg) {
        this.ErrMsg = errMsg;
    }

    public void setErrCode(String errCode) {
        this.ErrCode = errCode;
    }

    public void setResponseBody(String responseBody) {
        this.ResponseBody = responseBody;
    }
}
