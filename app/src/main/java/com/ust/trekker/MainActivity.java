package com.ust.trekker;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import com.ust.trekker.constant.App;
import com.ust.trekker.fragment.AboutUsFragment;
import com.ust.trekker.fragment.CustomFragment;
import com.ust.trekker.fragment.EventNewsFragment;
import com.ust.trekker.fragment.GameZoneFragment;
import com.ust.trekker.fragment.HarmonicLifeFragment;
import com.ust.trekker.fragment.HomeFragment;
import com.ust.trekker.fragment.NewsLetterFragment;
import com.ust.trekker.fragment.PhotoAlbumListFragment;
import com.ust.trekker.fragment.PhotoFragment;
import com.ust.trekker.fragment.StickyDateListFragment;
import com.ust.trekker.util.FragmentStack;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    ImageButton albumButton;
    View bottomMenu;
    int currFrag;
    ImageButton eventNewsButton;
    ImageButton gameZoneButton;
    ImageButton harmonicLifeButton;
    HomeFragment homeFragment;
    ImageButton newsLetterButton;
    int prevFrag;
    FragmentStack stack;

    public MainActivity() {
        this.stack = FragmentStack.getInstance();
        this.currFrag = 0;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            int color;
            if (App.ENV.equals(App.DEV_LOGIN_TOKEN)) {
                color = getResources().getColor(R.color.dev_action_bar);
            } else if (App.ENV.equals(App.UAT_LOGIN_TOKEN)) {
                color = getResources().getColor(R.color.uat_action_bar);
            } else {
                color = getResources().getColor(R.color.pro_action_bar);
            }
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        ((NavigationView) findViewById(R.id.nav_view)).setNavigationItemSelectedListener(this);
        init();
    }

    public void onResume() {
        super.onResume();
    }

    public void init() {
        if (this.homeFragment == null) {
            this.homeFragment = new HomeFragment();
        }
        this.homeFragment.setTitle(getString(R.string.home_label));
        if (this.stack.isEmpty()) {
            this.stack.add(this.homeFragment);
        }
        this.bottomMenu = findViewById(R.id.main_activity_bottom_menu);
        this.gameZoneButton = (ImageButton) findViewById(R.id.main_activity_game_zone);
        this.eventNewsButton = (ImageButton) findViewById(R.id.main_activity_event_news);
        this.newsLetterButton = (ImageButton) findViewById(R.id.main_activity_news_letter);
        this.albumButton = (ImageButton) findViewById(R.id.main_activity_album);
        this.harmonicLifeButton = (ImageButton) findViewById(R.id.main_activity_harmonic_life);
        this.eventNewsButton.setTag(Integer.valueOf(1));
        this.harmonicLifeButton.setTag(Integer.valueOf(2));
        this.newsLetterButton.setTag(Integer.valueOf(3));
        this.albumButton.setTag(Integer.valueOf(4));
        this.gameZoneButton.setTag(Integer.valueOf(5));
        this.gameZoneButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.stack.backToHome();
                CustomFragment fragment = new GameZoneFragment();
                fragment.setTitle(MainActivity.this.getString(R.string.game_zone_label));
                MainActivity.this.startFragment(fragment, ((Integer) view.getTag()).intValue());
            }
        });
        this.eventNewsButton.setOnClickListener(new  OnClickListener() {
            public void onClick(View view) {
                MainActivity.this.stack.backToHome();
                CustomFragment fragment = new EventNewsFragment();
                fragment.setTitle(MainActivity.this.getString(R.string.event_news_label));
                MainActivity.this.startFragment(fragment, ((Integer) view.getTag()).intValue());
            }
        });
        this.albumButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.stack.backToHome();
                CustomFragment fragment = new PhotoAlbumListFragment();
                fragment.setTitle(MainActivity.this.getString(R.string.photo_album_label));
                MainActivity.this.startFragment(fragment, ((Integer) view.getTag()).intValue());
            }
        });
        this.newsLetterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.stack.backToHome();
                CustomFragment fragment = new NewsLetterFragment();
                fragment.setTitle(MainActivity.this.getString(R.string.news_letter_label));
                MainActivity.this.startFragment(fragment, ((Integer) view.getTag()).intValue());
            }
        });
        this.harmonicLifeButton.setOnClickListener(new  OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.stack.backToHome();
                CustomFragment fragment = new HarmonicLifeFragment();
                fragment.setTitle(MainActivity.this.getString(R.string.harmonic_life_label));
                MainActivity.this.startFragment(fragment, ((Integer) view.getTag()).intValue());
            }
        });
        setMainFragment();
    }

    private void setMainFragment() {
        this.currFrag = this.prevFrag;
        this.prevFrag = -1;
        if (this.currFrag != this.prevFrag) {
            this.stack.clear();
            this.stack.add(this.homeFragment);
            setTitle(getString(R.string.home_label));
            getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content, this.homeFragment).commitAllowingStateLoss();
        }
    }

    public void startFragment(CustomFragment fragment, int dest) {
        if (this.currFrag != dest || this.currFrag == 11) {
            this.prevFrag = this.currFrag;
            this.currFrag = dest;
            if (this.currFrag != -1) {
                if (this.prevFrag >= this.currFrag) {
                    if (this.currFrag < this.prevFrag) {
                    }
                }
            }
            this.stack.add(fragment);
            super.setTitle(fragment.getTitle());
            getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_content, fragment).commitAllowingStateLoss();
        }
    }

    public void startFragment(CustomFragment fragment) {
        startFragment(fragment, 11);
    }

    public void switchFragment(CustomFragment fragment) {
        this.stack.pop();
        startFragment(fragment);
    }

    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen((int) GravityCompat.START)) {
            drawer.closeDrawer((int) GravityCompat.START);
        } else if (this.stack.size() > 1) {
            this.stack.pop();
            startFragment((CustomFragment) this.stack.pop(), this.prevFrag);
        } else {
            super.onBackPressed();
        }
    }

    private String[] getFragmentTitles() {
        return new String[]{getString(R.string.home_label), getString(R.string.event_news_label), getString(R.string.harmonic_life_label), getString(R.string.news_letter_label), getString(R.string.photo_album_label), getString(R.string.game_zone_label)};
    }

    public Fragment getFragmentById(int id) {
        switch (id) {
            case R.styleable.StickyListHeadersListView_android_scrollbarStyle /*0*/:
                return this.homeFragment;
            case R.styleable.StickyListHeadersListView_android_padding /*1*/:
                return new StickyDateListFragment();
            case R.styleable.StickyListHeadersListView_android_paddingLeft /*2*/:
                return new HarmonicLifeFragment();
            case R.styleable.StickyListHeadersListView_android_paddingRight /*4*/:
                return new PhotoFragment();
            case R.styleable.StickyListHeadersListView_android_paddingBottom /*5*/:
                return new GameZoneFragment();
            default:
                return this.homeFragment;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.action_login).setVisible(!App.login);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_login) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home /*2131493089*/:
                setMainFragment();
                break;
            case R.id.nav_event_news /*2131493090*/:
                this.eventNewsButton.callOnClick();
                break;
            case R.id.nav_news_letter /*2131493091*/:
                this.newsLetterButton.callOnClick();
                break;
            case R.id.nav_harmonic_life /*2131493092*/:
                this.harmonicLifeButton.callOnClick();
                break;
            case R.id.nav_album /*2131493093*/:
                this.albumButton.callOnClick();
                break;
            case R.id.nav_about_us /*2131493094*/:
                startFragment(new AboutUsFragment());
                break;
        }
        ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer((int) GravityCompat.START);
        return true;
    }

    public void hideActionbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
        this.bottomMenu.setVisibility(View.INVISIBLE);
    }

    public void showActionBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().show();
        }
        this.bottomMenu.setVisibility(View.VISIBLE);
    }
}
