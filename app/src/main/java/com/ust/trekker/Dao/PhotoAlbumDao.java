package com.ust.trekker.Dao;

import com.ust.trekker.cache.PhotoAlbumCache;
import com.ust.trekker.model.Photo;
import com.ust.trekker.model.PhotoAlbum;
import java.util.List;

public class PhotoAlbumDao {
    private static PhotoAlbumDao ourInstance;
    private PhotoAlbumCache photoAlbumCache;

    static {
        ourInstance = new PhotoAlbumDao();
    }

    private PhotoAlbumDao() {
        this.photoAlbumCache = PhotoAlbumCache.getInstance();
    }

    public static PhotoAlbumDao getInstance() {
        return ourInstance;
    }

    public int getLastUpdate() {
        return this.photoAlbumCache.getLastAlbumId();
    }

    public void addAlbum(PhotoAlbum album) {
        this.photoAlbumCache.addAlbum(album);
    }

    public void clearAlbum() {
        this.photoAlbumCache.clearAlbum();
    }

    public List<PhotoAlbum> getAlbums() {
        return this.photoAlbumCache.getAlbums();
    }

    public List<Photo> getPhotoByAlbumId(int id) {
        return this.photoAlbumCache.getAlbumThumbnails(id);
    }

    public void addPhotoToAlbum(Photo photo) {
        this.photoAlbumCache.addAlbumThumbnail(photo);
    }

    public List<Photo> getPhotosByArticleId(int articleId) {
        return this.photoAlbumCache.getArticleThumnbails(articleId);
    }

    public void addArticlePhoto(Photo photo) {
        this.photoAlbumCache.addArticleThumbnail(photo);
    }

    public Photo getPhotoByPhotoId(int photoId) {
        return this.photoAlbumCache.getPhotoByPhotoId(photoId);
    }

    public void addPhoto(Photo photo) {
        this.photoAlbumCache.addPhoto(photo);
    }

    public void clear() {
        this.photoAlbumCache.clear();
    }
}
