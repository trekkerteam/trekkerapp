package com.ust.trekker.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.ust.trekker.cache.CategoryCache;
import com.ust.trekker.db.DBContract;
import com.ust.trekker.db.DBContract.AppData;
import com.ust.trekker.model.Category;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CategoryDao extends Dao {
    private static CategoryDao categoryDao;
    private CategoryCache categoryCache;

    private CategoryDao(Context context) {
        super(context);
        this.categoryCache = CategoryCache.getInstance();
    }

    public static CategoryDao getInstance(Context context) {
        if (categoryDao == null) {
            categoryDao = new CategoryDao(context);
        }
        return categoryDao;
    }

    public static CategoryDao getInstance() {
        return categoryDao;
    }

    public int getLastUpdateId() {
        return super.getLastUpdateId(AppData.TABLE_CATEGORY);
    }

    public void updateLastId(int id) {
        super.updateLastId(AppData.TABLE_CATEGORY, id);
    }

    public Category getCategoryById(int id) {
        Cursor c = getReadableDB().rawQuery("SELECT * FROM Category WHERE catId=?", new String[]{String.valueOf(id)});
        c.close();
        closeDb();
        return c.moveToFirst() ? getContent(c) : null;
    }

    public List<Category> getCategoryByParentCatId(int id) {
        Cursor c = getReadableDB().rawQuery("SELECT * FROM Category WHERE parentCatId=? ORDER BY createDate DESC, catId DESC", new String[]{String.valueOf(id)});
        List<Category> categories = new ArrayList();
        while (c.moveToNext()) {
            categories.add(getContent(c));
        }
        c.close();
        closeDb();
        return categories;
    }

    public List<Category> getCategoryByParentCatIdReverse(int id) {
        Cursor c = getReadableDB().rawQuery("SELECT * FROM Category WHERE parentCatId=? ORDER BY createDate ASC, catId ASC", new String[]{String.valueOf(id)});
        List<Category> categories = new ArrayList();
        while (c.moveToNext()) {
            categories.add(getContent(c));
        }
        c.close();
        closeDb();
        return categories;
    }

    public List<Category> getCategoryByParentCatIdAndYear(int id, String year) {
        String sql = "SELECT * FROM Category WHERE parentCatId=? AND createDate LIKE '%" + year + "%' ORDER BY " + DBContract.Category.COLUMN_NAME_CREATE_DATE + " DESC, " + DBContract.Category.COLUMN_NAME_CAT_ID + " DESC";
        Cursor c = getReadableDB().rawQuery(sql, new String[]{String.valueOf(id)});
        List<Category> categories = new ArrayList();
        while (c.moveToNext()) {
            categories.add(getContent(c));
        }
        c.close();
        closeDb();
        Collections.sort(categories);
        return categories;
    }

    public void save(Category[] categories) {
        SQLiteDatabase db = getWritableDb();
        for (Category c : categories) {
            Cursor cursor = db.rawQuery("SELECT 1 FROM Category WHERE catId=?", new String[]{String.valueOf(c.getId())});
            if (cursor.moveToFirst()) {
                cursor.close();
            } else {
                cursor.close();
                ContentValues values = new ContentValues();
                values.put(DBContract.Category.COLUMN_NAME_PARENT_CAT_ID, Integer.valueOf(c.getParentCatId()));
                values.put(DBContract.Category.COLUMN_NAME_CAT_NAME, c.getCategorName());
                values.put(DBContract.Category.COLUMN_NAME_CAT_ID, Integer.valueOf(c.getId()));
                values.put(DBContract.Category.COLUMN_NAME_CREATE_DATE, c.getCreateDate());
                db.insert(DBContract.Category.TABLE_NAME, null, values);
            }
        }
        closeDb();
    }

    public void deleteByServerId(String ids) {
        super.deleteByServerId(DBContract.Category.TABLE_NAME, DBContract.Category.COLUMN_NAME_CAT_ID, ids);
    }

    public String getAllServerId() {
        return super.getAllServerId(DBContract.Category.TABLE_NAME, DBContract.Category.COLUMN_NAME_CAT_ID);
    }

    public Category getContent(Cursor c) {
        Category category = new Category();
        category.setId(c.getInt(c.getColumnIndexOrThrow(DBContract.Category.COLUMN_NAME_CAT_ID)));
        category.setParentCatId(c.getInt(c.getColumnIndexOrThrow(DBContract.Category.COLUMN_NAME_PARENT_CAT_ID)));
        category.setCategorName(c.getString(c.getColumnIndexOrThrow(DBContract.Category.COLUMN_NAME_CAT_NAME)));
        category.setCreateDate(c.getString(c.getColumnIndexOrThrow(DBContract.Category.COLUMN_NAME_CREATE_DATE)));
        return category;
    }
}
