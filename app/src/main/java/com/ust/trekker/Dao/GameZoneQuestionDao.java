package com.ust.trekker.Dao;

import com.ust.trekker.model.GameZoneQuestion;
import java.util.ArrayList;
import java.util.List;

public class GameZoneQuestionDao {
    private static GameZoneQuestionDao ourInstance;
    private List<GameZoneQuestion> gameZoneQuestions;

    static {
        ourInstance = new GameZoneQuestionDao();
    }

    public static GameZoneQuestionDao getInstance() {
        return ourInstance;
    }

    private GameZoneQuestionDao() {
        this.gameZoneQuestions = new ArrayList();
    }
}
