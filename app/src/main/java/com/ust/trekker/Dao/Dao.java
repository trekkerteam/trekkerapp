package com.ust.trekker.Dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;
import com.ust.trekker.db.DBContract.AppData;
import com.ust.trekker.db.DBHelper;
import java.util.concurrent.locks.ReentrantLock;
import uk.co.senab.photoview.BuildConfig;

public abstract class Dao {
    public static final String TAG = "DAO";
    private static final ReentrantLock lock;
    private Context context;
    private SQLiteDatabase db;
    private DBHelper helper;

    protected abstract Object getContent(Cursor cursor);

    static {
        lock = new ReentrantLock();
    }

    public Dao(Context context) {
        this.context = context;
    }

    protected SQLiteDatabase getWritableDb() {
        synchronized (lock) {
            Log.d(TAG, "waiting for lock (write db)...");
            lock.lock();
            Log.d(TAG, "lock acquired (write db)");
        }
        this.helper = new DBHelper(this.context);
        this.db = this.helper.getWritableDatabase();
        return this.db;
    }

    protected SQLiteDatabase getReadableDB() {
        synchronized (lock) {
            Log.d(TAG, "waiting for lock (read db).....");
            lock.lock();
            Log.d(TAG, "lock acquired (read db)");
        }
        this.helper = new DBHelper(this.context);
        this.db = this.helper.getReadableDatabase();
        return this.db;
    }

    protected void closeDb() {
        this.db.close();
        this.helper.close();
        lock.unlock();
        Log.d(TAG, "lock released.");
    }

    public int getLastUpdateId(String tableType) {
        Cursor c = getReadableDB().rawQuery("SELECT lastId FROM appData WHERE tableType=?", new String[]{tableType});
        if (c.moveToFirst()) {
            int tmp = c.getInt(c.getColumnIndexOrThrow(AppData.COLUMN_NAME_LATEST_ID));
            c.close();
            closeDb();
            return tmp;
        }
        c.close();
        closeDb();
        return 0;
    }

    public String getAllServerId() {
        throw new RuntimeException("Stub!");
    }

    public void deleteByServerId(String ids) {
        throw new RuntimeException("Stub!");
    }

    protected String getAllServerId(String tableName, String idName) {
        Cursor c = getReadableDB().rawQuery("SELECT " + idName + " FROM " + tableName, new String[0]);
        String ids = BuildConfig.FLAVOR;
        while (c.moveToNext()) {
            ids = ids + c.getInt(c.getColumnIndexOrThrow(idName)) + ",";
        }
        c.close();
        closeDb();
        if (TextUtils.isEmpty(ids)) {
            return ids;
        }
        return ids.substring(0, ids.length() - 1);
    }

    protected void deleteByServerId(String tableName, String idName, String ids) {
        getWritableDb().execSQL("DELETE FROM " + tableName + " WHERE " + idName + " IN (" + ids + ")");
        closeDb();
    }

    public void updateLastId(String tableType, int id) {
        getWritableDb().rawQuery("UPDATE appData SET lastId=? WHERE tableType=?", new String[]{tableType, String.valueOf(id)});
        closeDb();
    }
}
