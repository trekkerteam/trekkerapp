package com.ust.trekker.Dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.ust.trekker.constant.App;
import com.ust.trekker.db.DBContract;
import com.ust.trekker.db.DBContract.AppData;
import com.ust.trekker.model.Article;
import com.ust.trekker.model.Category;
import com.ust.trekker.util.Util;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import uk.co.senab.photoview.BuildConfig;

public class ArticleDao extends Dao {
    private static ArticleDao articleDao;

    public ArticleDao(Context context) {
        super(context);
    }

    public static ArticleDao getInstance() {
        return articleDao;
    }

    public static ArticleDao getInstance(Context context) {
        if (articleDao == null) {
            articleDao = new ArticleDao(context);
        }
        return articleDao;
    }

    public int getLastUpdateId() {
        return super.getLastUpdateId(AppData.TABLE_ARTICLE);
    }

    public void updateLastId(int id) {
        super.updateLastId(AppData.TABLE_ARTICLE, id);
    }

    public Article getArticleById(int id) {
        Cursor c = getReadableDB().rawQuery("SELECT * FROM article WHERE articleId=?", new String[]{String.valueOf(id)});
        return c.moveToFirst() ? getContent(c) : null;
    }

    public List<Article> getArticleByCategoryIdCascade(int id, CategoryDao categoryDao) {
        List<Article> articles = new ArrayList();
        getArticleByCategoryIdCascade(id, articles, categoryDao);
        Collections.sort(articles, new Comparator<Article>() {
            @Override
            public int compare(Article lhs, Article rhs) {
                SimpleDateFormat format = new SimpleDateFormat(Util.getDateFormat(lhs.getCreateDate()), Locale.ENGLISH);
                try {
                    return format.parse(rhs.getCreateDate()).compareTo(format.parse(lhs.getCreateDate()));
                } catch (ParseException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
        return articles;
    }

    public void getArticleByCategoryIdCascade(int id, List<Article> articles, CategoryDao categoryDao) {
        List<Category> categories = categoryDao.getCategoryByParentCatId(id);
        if (categories == null || categories.size() <= 0) {
            articles.addAll(getArticleByCategoryId(id));
            return;
        }
        for (Category category : categories) {
            getArticleByCategoryIdCascade(category.getId(), articles, categoryDao);
        }
    }

    public List<Article> getArticleByCategoryId(int id) {
        List<Article> articles = new ArrayList();
        Cursor c = getReadableDB().rawQuery("SELECT * FROM article WHERE categoryId=? ORDER BY createDate DESC, articleId DESC", new String[]{String.valueOf(id)});
        while (c.moveToNext()) {
            articles.add(getContent(c));
        }
        c.close();
        closeDb();
        return articles;
    }

    public List<Article> getArticleByCategoryIdAndYear(int id, int year) {
        String sql = "SELECT * FROM article WHERE categoryId=? AND createDate LIKE '%" + year + "%' " + "ORDER BY " + DBContract.Category.COLUMN_NAME_CREATE_DATE + " DESC, " + DBContract.Article.COLUMN_NAME_ARTICLE_ID + " DESC";
        List<Article> articles = new ArrayList();
        Cursor c = getReadableDB().rawQuery(sql, new String[]{String.valueOf(id)});
        while (c.moveToNext()) {
            articles.add(getContent(c));
        }
        c.close();
        closeDb();
        return articles;
    }

    public void save(Article[] articles) {
        SQLiteDatabase db = getWritableDb();
        for (Article a : articles) {
            Cursor cursor = db.rawQuery("SELECT 1 FROM Article WHERE articleId=?", new String[]{String.valueOf(a.getId())});
            if (cursor.moveToFirst()) {
            } else {
                String createDate;
                try {
                    createDate = Util.convertDate(App.SERVER_DATE_FORMAT, App.SQLITE_DATE_FORMAT, a.getCreateDate());
                } catch (Exception e) {
                    e.printStackTrace();
                    createDate = a.getCreateDate();
                }
                ContentValues values = new ContentValues();
                values.put(DBContract.Article.COLUMN_NAME_ARTICLE_ID, Integer.valueOf(a.getId()));
                values.put(DBContract.Article.COLUMN_NAME_CATEGORY_ID, Integer.valueOf(a.getCategoryId()));
                values.put(DBContract.Article.COLUMN_NAME_TITLE, a.getTitle());
                values.put(DBContract.Article.COLUMN_NAME_NEWS_DATE, a.getNewsDate());
                values.put(DBContract.Article.COLUMN_NAME_NEWS_LINK, a.getNewsLink());
                values.put(DBContract.Article.COLUMN_NAME_NEWS_SOURCE, a.getNewsSource());
                values.put(DBContract.Article.COLUMN_NAME_CONTENT, a.getContent());
                values.put(DBContract.Category.COLUMN_NAME_CREATE_DATE, createDate);
                values.put(DBContract.Article.COLUMN_NAME_PHOTO_IDS, a.getPhotoIds());
                db.insert(DBContract.Article.TABLE_NAME, null, values);
            }
            cursor.close();
        }

        closeDb();
    }

    public void deleteByServerId(String ids) {
        super.deleteByServerId(DBContract.Article.TABLE_NAME, DBContract.Article.COLUMN_NAME_ARTICLE_ID, ids);
    }

    public String getAllServerId() {
        return super.getAllServerId(DBContract.Article.TABLE_NAME, DBContract.Article.COLUMN_NAME_ARTICLE_ID);
    }

    protected Article getContent(Cursor c) {
        Article article = new Article();
        article.setId(c.getInt(c.getColumnIndexOrThrow(DBContract.Article.COLUMN_NAME_ARTICLE_ID)));
        article.setCategoryId(c.getInt(c.getColumnIndexOrThrow(DBContract.Article.COLUMN_NAME_CATEGORY_ID)));
        article.setContent(c.getString(c.getColumnIndexOrThrow(DBContract.Article.COLUMN_NAME_CONTENT)));
        article.setTitle(c.getString(c.getColumnIndexOrThrow(DBContract.Article.COLUMN_NAME_TITLE)));
        article.setCreateDate(c.getString(c.getColumnIndexOrThrow(DBContract.Category.COLUMN_NAME_CREATE_DATE)));
        article.setPhotoIds(c.getString(c.getColumnIndexOrThrow(DBContract.Article.COLUMN_NAME_PHOTO_IDS)));
        article.setNewsSource(c.getString(c.getColumnIndexOrThrow(DBContract.Article.COLUMN_NAME_NEWS_SOURCE)));
        article.setNewsDate(c.getString(c.getColumnIndexOrThrow(DBContract.Article.COLUMN_NAME_NEWS_DATE)));
        article.setNewsLink(c.getString(c.getColumnIndexOrThrow(DBContract.Article.COLUMN_NAME_NEWS_LINK)));
        return article;
    }
}
