package com.ust.trekker.constant;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class App {
    public static final String DEV_LOGIN_TOKEN = "dev";
    public static final String E000 = "000";
    public static final String E111 = "111";
    public static final String ENV = "prod";
    public static final int EVENT_NEWS_FRAGMENT = 1;
    public static int EVEN_ID = 0;
    public static final String FILE_DIRECTORY = "trekker";
    public static final int GAME_ZONE_FRAGMENT = 5;
    public static final int HARMONIC_LIFE_FRAGMENT = 2;
    public static int HARM_ID = 0;
    public static final int HOME_FRAGMENT = 0;
    public static int NEWS_ID = 0;
    public static final int NEWS_LETTER_FRAGMENT = 3;
    public static final int PAGE_CONTENT = 11;
    public static final int PHOTO_FRAGMENT = 4;
    public static final String REGISTRATION_COMPLETE = "registrationCompleted";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String SERVER_DATE_FORMAT = "MM/dd/yyyy h:mm:ss a";
    public static final String SQLITE_DATE_FORMAT = "yyyy/MM/dd";
    public static final int THUMBNAIL_HEIGHT = 200;
    public static final int THUMBNAIL_WIDTH = 200;
    public static final String UAT_LOGIN_TOKEN = "uat";
    public static final String URL_ENCODE_FORMAT = "UTF-8";
    public static int _DB_VERSION = 21;
    public static final String bypassLogin = "@0011";
    public static String cred = null;
    public static String database = "trekker.db";;
    public static boolean login = false;
    public static final int minYear = 2016;
    public static final String noImage = "@no_image";

    static {
        NEWS_ID = EVENT_NEWS_FRAGMENT;
        HARM_ID = HARMONIC_LIFE_FRAGMENT;
        EVEN_ID = NEWS_LETTER_FRAGMENT;
        database = "trekker.db";
        _DB_VERSION = 21;
    }

    public static String formatDate(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd h:mm:ss", Locale.ENGLISH).format(date);
    }
}
