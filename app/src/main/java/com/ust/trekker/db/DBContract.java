package com.ust.trekker.db;

import android.provider.BaseColumns;

public class DBContract {

    public static abstract class AppData implements BaseColumns {
        public static final String COLUMN_NAME_LATEST_ID = "lastId";
        public static final String COLUMN_NAME_TABLE_TYPE = "tableType";
        public static final String TABLE_ALBUM = "lastAlbum";
        public static final String TABLE_ARTICLE = "lastArticle";
        public static final String TABLE_CATEGORY = "lastCategory";
        public static final String TABLE_NAME = "appData";
        public static final String TABLE_PHOTO = "lastPhoto";
    }

    public static abstract class Article implements BaseColumns {
        public static final String COLUMN_NAME_ARTICLE_ID = "articleId";
        public static final String COLUMN_NAME_CATEGORY_ID = "categoryId";
        public static final String COLUMN_NAME_CONTENT = "content";
        public static final String COLUMN_NAME_CREATE_DATE = "createDate";
        public static final String COLUMN_NAME_NEWS_DATE = "newsDate";
        public static final String COLUMN_NAME_NEWS_LINK = "newsLink";
        public static final String COLUMN_NAME_NEWS_SOURCE = "newsSource";
        public static final String COLUMN_NAME_PHOTO_IDS = "photoIds";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String TABLE_NAME = "Article";
    }

    public static abstract class Category implements BaseColumns {
        public static final String COLUMN_NAME_CAT_ID = "catId";
        public static final String COLUMN_NAME_CAT_NAME = "categoryName";
        public static final String COLUMN_NAME_CREATE_DATE = "createDate";
        public static final String COLUMN_NAME_PARENT_CAT_ID = "parentCatId";
        public static final String TABLE_NAME = "Category";
    }
}
