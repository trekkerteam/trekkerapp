package com.ust.trekker.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.ust.trekker.constant.App;
import com.ust.trekker.db.DBContract.AppData;

public class DBHelper extends SQLiteOpenHelper {
    public static final String TAG = "dbHelper";
    Context context;

    public DBHelper(Context context) {
        super(context, App.database, null, App._DB_VERSION);
        this.context = context;
    }

    public void onCreate(SQLiteDatabase db) {
        int i = 0;
        String createCategoryTable = "CREATE TABLE IF NOT EXISTS Category(_id INTEGER PRIMARY KEY, categoryName NVARCHAR(50), parentCatId INT, catId INT, createDate DATE, FOREIGN KEY (parentCatId) REFERENCES Category (_id) ON DELETE CASCADE)";
        Log.d(TAG, createCategoryTable);
        db.execSQL(createCategoryTable);
        String createArticleTable = "CREATE TABLE IF NOT EXISTS Article (_id INTEGER PRIMARY KEY, articleId INTEGER, categoryId INT, photoIds VARCHAR(30), title TEXT, newsDate VARCHAR(20), newsLink NVARCHAR(30), newsSource NVARCHAR(20), content TEXT, createDate DATE, FOREIGN KEY (categoryId) REFERENCES Category (_id) ON DELETE CASCADE)";
        Log.d(TAG, createArticleTable);
        db.execSQL(createArticleTable);
        String createAppDataTable = "CREATE TABLE IF NOT EXISTS appData(tableType VARCHAR(20), lastId INTEGER)";
        Log.d(TAG, createAppDataTable);
        db.execSQL(createAppDataTable);
        String[] tmps = new String[]{AppData.TABLE_CATEGORY, AppData.TABLE_ARTICLE, AppData.TABLE_ALBUM, AppData.TABLE_PHOTO};
        int length = tmps.length;
        while (i < length) {
            String tmp = tmps[i];
            ContentValues values = new ContentValues();
            values.put(AppData.COLUMN_NAME_TABLE_TYPE, tmp);
            values.put(AppData.COLUMN_NAME_LATEST_ID, Integer.valueOf(-1));
            db.insert(AppData.TABLE_NAME, null, values);
            i++;
        }
    }

    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        switch (i) {
            case 20:
                String sql = "ALTER TABLE Category ADD createDate VARCHAR(30)";
                Log.d(TAG, sql);
                db.execSQL(sql);
            default:
        }
    }
}
