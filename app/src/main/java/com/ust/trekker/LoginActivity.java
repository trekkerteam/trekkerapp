package com.ust.trekker;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Response;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.net.ServerConnector.ServerDownloadCallback;
import com.ust.trekker.view.ViewFactory;

public class LoginActivity extends AppCompatActivity {
    boolean btnLock;
    TextView credTxtVw;
    ProgressDialog loadingBar;
    View loginBtn;
    boolean shouldLeave;
    String token;

    public LoginActivity() {
        this.btnLock = false;
        this.token = null;
        this.shouldLeave = false;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (getSupportActionBar() != null) {
            int color;
            if (App.ENV.equals(App.DEV_LOGIN_TOKEN)) {
                color = getResources().getColor(R.color.dev_action_bar);
            } else if (App.ENV.equals(App.UAT_LOGIN_TOKEN)) {
                color = getResources().getColor(R.color.uat_action_bar);
            } else {
                color = getResources().getColor(R.color.pro_action_bar);
            }
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
        }
        this.token = loadCred(this);
        this.loginBtn = findViewById(R.id.login_activity_login);
        this.credTxtVw = (TextView) findViewById(R.id.login_activity_cred);
        this.credTxtVw.setText(this.token);
        this.credTxtVw.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                if (s.length() > 10) {
                    LoginActivity.this.credTxtVw.setError(LoginActivity.this.getResources().getString(R.string.cred_warning));
                }
                LoginActivity.this.loginBtn.setEnabled(s.length() <= 10);
            }
        });
        this.loginBtn.setOnClickListener(new OnClickListener() {

            public void onClick(View view) {
                LoginActivity.this.btnLock = true;
                LoginActivity.this.login(LoginActivity.this.credTxtVw.getText().toString());
            }
        });
        if (this.token != null) {
            login(this.token);
        }
    }

    public void login(final String token) {
        if (token.equals(App.bypassLogin)) {
            saveCred(token);
            startActivity(new Intent(this, SplashActivity.class));
            finish();
            return;
        }
        startLoading(getString(R.string.system_msg_login));
        if (token != null && (token.contains(App.DEV_LOGIN_TOKEN) || App.ENV.equals(App.DEV_LOGIN_TOKEN))) {
            ServerConnector.setDev();
        } else if (token == null || !(token.contains(App.UAT_LOGIN_TOKEN) || App.ENV.equals(App.UAT_LOGIN_TOKEN))) {
            ServerConnector.setPro();
        } else {
            ServerConnector.setUat();
        }
        try {
            new ServerConnector().userLogin(token, new ServerDownloadCallback() {

                @Override
                public void onDownloadFinished(Response response) {
                }

                @Override
                public void onDownloadFinishedUI(Response response) {
                    if (response.getErrCode().equals(App.E000)) {
                        try {
                            if (LoginActivity.this.getPackageManager().getPackageInfo(LoginActivity.this.getPackageName(), 0).versionCode < Integer.parseInt(response.getResponseBody())) {
                                ViewFactory.versionWarnning(LoginActivity.this, LoginActivity.this.getString(R.string.version_warnning), LoginActivity.this).show();
                                LoginActivity.this.shouldLeave = true;
                            } else {
                                App.login = true;
                                LoginActivity.this.saveCred(token);
                                LoginActivity.this.startActivity(new Intent(LoginActivity.this, SplashActivity.class));
                                LoginActivity.this.finish();
                            }
                        } catch (NameNotFoundException e) {
                            e.printStackTrace();
                            ViewFactory.versionWarnning(LoginActivity.this, LoginActivity.this.getString(R.string.version_warnning), LoginActivity.this).show();
                            LoginActivity.this.shouldLeave = true;
                        }
                    } else {
                        ViewFactory.warnningMsg(LoginActivity.this, response.getResponseBody()).show();
                    }
                    LoginActivity.this.stopLoading();
                    return;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveCred(String cred) {
        Editor editor = getSharedPreferences("config", 0).edit();
        editor.putString("cred", cred);
        editor.apply();
    }

    public static String loadCred(Context context) {
        return context.getSharedPreferences("config", 0).getString("cred", null);
    }

    public void startLoading(String dialog) {
        if (this.loadingBar != null) {
            this.loadingBar.dismiss();
        }
        this.loadingBar = ProgressDialog.show(this, getString(R.string.system_msg_label), dialog);
    }

    public void stopLoading() {
        if (this.loadingBar != null) {
            this.loadingBar.dismiss();
            this.btnLock = false;
        }
    }
}
