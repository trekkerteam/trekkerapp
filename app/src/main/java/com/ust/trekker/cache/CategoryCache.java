package com.ust.trekker.cache;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.ust.trekker.constant.App;
import com.ust.trekker.db.DBContract;
import com.ust.trekker.model.Category;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CategoryCache {
    private static CategoryCache categoryCache;
    private Map<Integer, List<Category>> categoryMap;

    static {
        categoryCache = new CategoryCache();
    }

    public CategoryCache() {
        this.categoryMap = new HashMap();
    }

    public static CategoryCache getInstance() {
        return categoryCache;
    }

    public List<Category> getHarmonicCategory() {
        return (List) this.categoryMap.get(Integer.valueOf(App.HARM_ID));
    }

    public List<Category> getNewsLetterCategory() {
        return (List) this.categoryMap.get(Integer.valueOf(App.NEWS_ID));
    }

    public List<Category> getCategoryByParentId(int id) {
        return (List) this.categoryMap.get(Integer.valueOf(id));
    }

//    public int getIdByCatName(String catName) {
//        for (Entry<Integer, List<Category>> entry : this.categoryMap.entrySet()) {
//            for (Category c : (List) entry.getValue()) {
//                if (c.getCategorName().equals(catName)) {
//                    return c.getId();
//                }
//            }
//        }
//        return -1;
//    }

    public void clear() {
        this.categoryMap.clear();
    }

    public void load(SQLiteDatabase db) {
        Cursor c = db.rawQuery("SELECT * FROM Category", new String[0]);
        while (c.moveToNext()) {
            List<Category> categories;
            Category category = new Category();
            category.setCategorName(c.getString(c.getColumnIndexOrThrow(DBContract.Category.COLUMN_NAME_CAT_NAME)));
            category.setParentCatId(c.getInt(c.getColumnIndexOrThrow(DBContract.Category.COLUMN_NAME_PARENT_CAT_ID)));
            category.setId(c.getInt(c.getColumnIndexOrThrow(DBContract.Category.COLUMN_NAME_CAT_ID)));
            if (this.categoryMap.containsKey(Integer.valueOf(category.getParentCatId()))) {
                categories = (List) this.categoryMap.get(Integer.valueOf(category.getParentCatId()));
            } else {
                categories = new ArrayList();
            }
            categories.add(category);
            this.categoryMap.put(Integer.valueOf(category.getParentCatId()), categories);
        }
    }
}
