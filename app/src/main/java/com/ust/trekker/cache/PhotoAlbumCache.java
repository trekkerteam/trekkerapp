package com.ust.trekker.cache;

import com.ust.trekker.model.Photo;
import com.ust.trekker.model.PhotoAlbum;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhotoAlbumCache {
    private static PhotoAlbumCache photoAlbumCache;
    private Map<Integer, List<Photo>> albumThumbnails;
    private List<PhotoAlbum> albums;
    private Map<Integer, List<Photo>> articleThumbnails;
    private int lastAlbumId;
    private Map<Integer, Photo> photos;

    static {
        photoAlbumCache = new PhotoAlbumCache();
    }

    private PhotoAlbumCache() {
        this.albumThumbnails = new HashMap();
        this.articleThumbnails = new HashMap();
        this.photos = new HashMap();
        this.albums = new ArrayList();
        this.lastAlbumId = -1;
    }

    public static PhotoAlbumCache getInstance() {
        return photoAlbumCache;
    }

    public void addAlbum(PhotoAlbum album) {
        if (album.getId() > this.lastAlbumId) {
            this.lastAlbumId = album.getId();
        }
        this.albums.add(album);
    }

    public void clearAlbum() {
        this.albums.clear();
    }

    public void clear() {
        clearAlbum();
        this.photos.clear();
    }

    public List<PhotoAlbum> getAlbums() {
        return this.albums;
    }

    public int getLastAlbumId() {
        return this.lastAlbumId;
    }

    public List<Photo> addArticleThumbnail(Photo photo) {
        List<Photo> tmp = this.articleThumbnails.containsKey(Integer.valueOf(photo.getArticleId())) ? (List) this.articleThumbnails.get(Integer.valueOf(photo.getArticleId())) : new ArrayList();
        tmp.add(photo);
        this.articleThumbnails.put(Integer.valueOf(photo.getArticleId()), tmp);
        return tmp;
    }

    public List<Photo> addAlbumThumbnail(Photo photo) {
        List<Photo> tmp = this.albumThumbnails.containsKey(Integer.valueOf(photo.getAlbumId())) ? (List) this.albumThumbnails.get(Integer.valueOf(photo.getAlbumId())) : new ArrayList();
        tmp.add(photo);
        this.albumThumbnails.put(Integer.valueOf(photo.getAlbumId()), tmp);
        return tmp;
    }

    public List<Photo> getAlbumThumbnails(int id) {
        return (List) this.albumThumbnails.get(Integer.valueOf(id));
    }

    public List<Photo> getArticleThumnbails(int id) {
        return (List) this.articleThumbnails.get(Integer.valueOf(id));
    }

    public Photo getPhotoByPhotoId(int photoId) {
        return (Photo) this.photos.get(Integer.valueOf(photoId));
    }

    public void addPhoto(Photo photo) {
        this.photos.put(Integer.valueOf(photo.getId()), photo);
    }
}
