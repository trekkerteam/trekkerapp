package com.ust.trekker.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderLayout.PresetIndicators;
import com.daimajia.slider.library.SliderLayout.Transformer;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.OnSliderClickListener;
import com.daimajia.slider.library.SliderTypes.BaseSliderView.ScaleType;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx.OnPageChangeListener;
import com.google.gson.Gson;
import com.ust.trekker.R;
import com.ust.trekker.Dao.PhotoAlbumDao;
import com.ust.trekker.MainActivity;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Photo;
import com.ust.trekker.model.PhotoAlbum;
import com.ust.trekker.model.Response;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.net.ServerConnector.ServerDownloadCallback;
import com.ust.trekker.util.Util;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PhotoAlbumListFragment extends CustomFragment implements OnSliderClickListener, OnPageChangeListener {
    private ArrayAdapter adapter;
    private List<AlbumTextSlideView> albumCoverViews;
    private ListView albumListView;
    private ProgressBar loadingBar;
    private SliderLayout mDemoSlider;
    private PhotoAlbumDao photoAlbumDao;
    private List<PhotoAlbum> photoAlbums;
    private View root;
    
    private class AlbumTextSlideView extends TextSliderView {
        private PhotoAlbum photoAlbum;

        public AlbumTextSlideView(Context context) {
            super(context);
        }

        public PhotoAlbum getPhotoAlbum() {
            return this.photoAlbum;
        }

        public void setPhotoAlbum(PhotoAlbum photoAlbum) {
            this.photoAlbum = photoAlbum;
        }
    }

    public PhotoAlbumListFragment() {
        this.photoAlbumDao = PhotoAlbumDao.getInstance();
        this.photoAlbums = new ArrayList<>();
        this.albumCoverViews = new ArrayList<>();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_photo_album, null);
        this.mDemoSlider = (SliderLayout) this.root.findViewById(R.id.slider);
        this.albumListView = (ListView) this.root.findViewById(R.id.transformers);
        this.loadingBar = (ProgressBar) this.root.findViewById(R.id.loading_bar);
        this.adapter = new  ArrayAdapter<PhotoAlbum> (getActivity(), R.layout.simple_text_view, this.photoAlbums) {
            @Override
            public View getView(int i, View convertView, ViewGroup viewGroup) {
                if (convertView == null) {
                    convertView = ((LayoutInflater) PhotoAlbumListFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.simple_text_view, null);
                }
                final PhotoAlbum photoAlbum = (PhotoAlbum) getItem(i);
                ((TextView) convertView).setText(photoAlbum.getName());
                convertView.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        PhotoAlbumListFragment.this.onAlbumSelected(photoAlbum);
                    }
                });
                return convertView;
            }
        };
        if (Util.haveNetwork(getActivity())) {
            this.albumListView.setAdapter(this.adapter);
            checkUpdate();
            return this.root;
        }
        this.root.findViewById(R.id.only_available_online).setVisibility(View.VISIBLE);
        this.loadingBar.setVisibility(View.INVISIBLE);
        return this.root;
    }

    public void onDestroy() {
        super.onDestroy();
        this.photoAlbumDao.clear();
    }

    public void checkUpdate() {
        int lastId = this.photoAlbumDao.getLastUpdate();
        new ServerConnector().getAllAlbum(-1, new ServerDownloadCallback() {
            @Override
            public void onDownloadFinished(Response response) {
                if (response.ErrCode.equals(App.E000)) {
                    PhotoAlbum[] albums = (PhotoAlbum[]) new Gson().fromJson(response.getResponseBody(), PhotoAlbum[].class);
                    if (albums != null) {
                        PhotoAlbumListFragment.this.photoAlbumDao.clearAlbum();
                        for (PhotoAlbum pa : albums) {
                            PhotoAlbumListFragment.this.photoAlbumDao.addAlbum(pa);
                        }
                    }
                }
            }
            
            @Override
            public void onDownloadFinishedUI(Response response) {
                PhotoAlbumListFragment.this.loadData();
            }
        });
    }

    public void loadData() {
        this.photoAlbums.clear();
        this.photoAlbums.addAll(this.photoAlbumDao.getAlbums());
        this.adapter.notifyDataSetChanged();
        this.albumCoverViews.clear();
        this.mDemoSlider.setPresetTransformer(Transformer.Accordion);
        this.mDemoSlider.setPresetIndicator(PresetIndicators.Center_Bottom);
        this.mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        this.mDemoSlider.setDuration(8000);
        this.mDemoSlider.addOnPageChangeListener(this);
        loadCoverRecursive(this.photoAlbums, 0);
        this.loadingBar.setVisibility(View.GONE);
    }

    public void onAlbumSelected(PhotoAlbum photoAlbum) {
        PhotoAlbumContentFragment fragment = new PhotoAlbumContentFragment();
        fragment.setAlbum(photoAlbum);
        ((MainActivity) getActivity()).startFragment(fragment);
    }

    public void onSliderClick(BaseSliderView slider) {
        onAlbumSelected(((AlbumTextSlideView) slider).getPhotoAlbum());
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
    }

    public void onPageScrollStateChanged(int state) {
    }

    public void loadCoverRecursive(final List<PhotoAlbum> photoAlbums, final int counter) {
        if (counter >= 3 || photoAlbums.size() == counter) {
            onCoverLoaded();
            return;
        }
        final PhotoAlbum pa = (PhotoAlbum) photoAlbums.get(counter);
        String[] tmp = pa.getPhotoIds().split(",");
        final String cc = pa.getCoverContent();
        if (TextUtils.isEmpty(cc) && !TextUtils.isEmpty(pa.getPhotoIds())) {
            new ServerConnector().query(ServerConnector.DOWNLOAD_THUMBNAIL_BY_PHOTO_ID, new String[]{"photoId=" + tmp[0]}, new ServerDownloadCallback() {

                @Override
                public void onDownloadFinished(Response response) {
                }

                @Override
                public void onDownloadFinishedUI(Response response) {
                    try {
                        File file = Util.binaryToFile(PhotoAlbumListFragment.this.getActivity(), Base64.decode(Util.extractPhotoString(((Photo) new Gson().fromJson(response.getResponseBody(), Photo.class)).getContent()), 0), pa.getName() + ".jpg");
                        AlbumTextSlideView textSliderView = new AlbumTextSlideView(PhotoAlbumListFragment.this.getActivity());
                        textSliderView.description(pa.getName()).image(file).setScaleType(ScaleType.Fit).setOnSliderClickListener(PhotoAlbumListFragment.this);
                        textSliderView.setPhotoAlbum(pa);
                        textSliderView.bundle(new Bundle());
                        textSliderView.getBundle().putString("extra", cc);
                        albumCoverViews.add(textSliderView);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    loadCoverRecursive(photoAlbums, counter + 1);


                }
            });
        }
    }

    public void onCoverLoaded() {
        for (AlbumTextSlideView slideView : this.albumCoverViews) {
            this.mDemoSlider.addSlider(slideView);
        }
    }

    public void loadCover() {
        for (int i = 0; i < 3; i++) {
            final PhotoAlbum pa = (PhotoAlbum) this.photoAlbums.get(i);
            final String cc = pa.getCoverContent();
            if (TextUtils.isEmpty(cc) && !TextUtils.isEmpty(pa.getPhotoIds())) {
                String[] tmp = pa.getPhotoIds().split(",");
                new ServerConnector().query(ServerConnector.DOWNLOAD_THUMBNAIL_BY_PHOTO_ID, new String[]{"photoId=" + tmp[0]}, new ServerDownloadCallback() {
                    @Override
                    public void onDownloadFinished(Response response) {
                    }

                    @Override
                    public void onDownloadFinishedUI(Response response) {
                        try {
                            if (response.getErrCode().equals(App.E000)) {
                                File file = Util.binaryToFile(PhotoAlbumListFragment.this.getActivity(), Base64.decode(Util.extractPhotoString(((Photo) new Gson().fromJson(response.getResponseBody(), Photo.class)).getContent()), 0), pa.getName() + ".jpg");
                                AlbumTextSlideView textSliderView = new AlbumTextSlideView(PhotoAlbumListFragment.this.getActivity());
                                textSliderView.description(pa.getName()).image(file).setScaleType(ScaleType.Fit).setOnSliderClickListener(PhotoAlbumListFragment.this);
                                textSliderView.setPhotoAlbum(pa);
                                textSliderView.bundle(new Bundle());
                                textSliderView.getBundle().putString("extra", cc);
                                PhotoAlbumListFragment.this.mDemoSlider.addSlider(textSliderView);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
        this.mDemoSlider.setPresetTransformer(Transformer.Accordion);
        this.mDemoSlider.setPresetIndicator(PresetIndicators.Center_Bottom);
        this.mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        this.mDemoSlider.setDuration(4000);
        this.mDemoSlider.addOnPageChangeListener(this);
    }
}
