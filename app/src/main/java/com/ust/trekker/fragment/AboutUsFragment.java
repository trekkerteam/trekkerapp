package com.ust.trekker.fragment;

import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ust.trekker.R;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Response;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.net.ServerConnector.ServerDownloadCallback;
import com.ust.trekker.util.Util;

public class AboutUsFragment extends CustomFragment implements AboutUsFragmentCallback {
    String aboutUs;
    TextView aboutUsView;
    TextView appVersionView;
    View root;

  

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.about_us_label));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_about_us, null);
        this.aboutUsView = (TextView) this.root.findViewById(R.id.about_us);
        this.appVersionView = (TextView) this.root.findViewById(R.id.app_version);
        if (Util.haveNetwork(getActivity())) {
            downloadAboutUs();
            try {
                this.appVersionView.setText(String.valueOf(getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode));
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            this.aboutUs = getString(R.string.only_available_online);
        }
        return this.root;
    }

    public void downloadAboutUs() {
        new ServerConnector().downloadAboutUs(new ServerDownloadCallback() {
            public void onDownloadFinished(Response response) {
                if (response.getErrCode().equals(App.E000)) {
                    AboutUsFragment.this.aboutUs = response.getResponseBody();
                }
            }

            public void onDownloadFinishedUI(Response response) {
                AboutUsFragment.this.aboutUsView.setText(AboutUsFragment.this.aboutUs);
            }
        });
    }

    public void show() {
        this.aboutUsView.setText(this.aboutUs);
    }
}
