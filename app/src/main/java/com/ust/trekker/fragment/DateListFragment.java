package com.ust.trekker.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.ust.trekker.R;
import com.ust.trekker.listener.ListItemEventListener;
import com.ust.trekker.model.Article;
import java.util.ArrayList;
import java.util.List;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public abstract class DateListFragment extends CustomFragment {
    protected List<Article> articles;
    protected DateListAdapter dateListAdapter;
    protected StickyListHeadersListView listView;
    ListItemEventListener listener;
    protected View root;

    private class DateListAdapter<T> extends ArrayAdapter implements StickyListHeadersAdapter {
        Context context;
        List<T> list;

        public DateListAdapter(Context context, List<T> list) {
            super(context, -1, list);
            this.context = context;
            this.list = list;
        }

        public View getHeaderView(int position, View convertView, ViewGroup parent) {
            Article item = (Article) getItem(position);
            long id = getHeaderId(position);
            if (convertView == null) {
                return ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.sticky_header_item, null);
            }
            return convertView;
        }

        public long getHeaderId(int position) {
            return (long) position;
        }

        public View getView(int i, View convertView, ViewGroup viewGroup) {
            return DateListFragment.this.genContentView(i, convertView, viewGroup, getItem(i));
        }
    }

    public abstract View genContentView(int i, View view, ViewGroup viewGroup, Object obj);

    public abstract View genHeaderView(int i, View view, ViewGroup viewGroup);

    public DateListFragment() {
        this.articles = new ArrayList();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_date_list_layout, null);
        this.dateListAdapter = new DateListAdapter(getActivity(), this.articles);
        this.listView = (StickyListHeadersListView) this.root.findViewById(R.id.news_letter_listview);
        this.listView.setAdapter(this.dateListAdapter);
        return this.root;
    }

    public void setListener(ListItemEventListener listener) {
        this.listener = listener;
    }

    public void notifyDataSetChange() {
        if (this.dateListAdapter != null) {
            this.dateListAdapter.notifyDataSetChanged();
        }
    }
}
