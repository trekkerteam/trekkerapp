package com.ust.trekker.fragment;

import android.os.Bundle;
import com.ust.trekker.constant.App;

public class EventNewsFragment extends StickyDateListFragment {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setCategoryId(App.EVEN_ID);
    }
}
