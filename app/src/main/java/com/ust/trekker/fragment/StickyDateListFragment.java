package com.ust.trekker.fragment;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.gson.Gson;
import com.ust.trekker.R;
import com.ust.trekker.Dao.ArticleDao;
import com.ust.trekker.MainActivity;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Article;
import com.ust.trekker.model.Photo;
import com.ust.trekker.model.Response;
import com.ust.trekker.net.DownloadDataHelper;
import com.ust.trekker.net.DownloadDataHelper.Callback;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.net.ServerConnector.ServerDownloadCallback;
import com.ust.trekker.util.LayoutFactory;
import com.ust.trekker.util.LayoutFactory.SimpleLayout;
import com.ust.trekker.util.Util;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import uk.co.senab.photoview.BuildConfig;
import uk.co.senab.photoview.IPhotoView;

public class StickyDateListFragment extends CustomFragment implements StickyDateListFragmentCallback {
    DateListAdapter adapter;
    ArticleDao articleDao;
    List<Article> articles;
    int categoryId;
    private int curYear;
    View leftBtn;
    StickyListHeadersListView listView;
    ProgressBar loadingBar;
    View rightBtn;
    View root;
    TextView yearView;

    class DateListAdapter extends ArrayAdapter<Article> implements StickyListHeadersAdapter {
        Context context;
        List<Article> list;

        public DateListAdapter(Context context, List<Article> list) {
            super(context, -1, list);
            this.context = context;
            this.list = list;
        }

        public View getHeaderView(int position, View convertView, ViewGroup parent) {
            Object item = getItem(position);
            String header = getHeader(position);
            if (convertView == null) {
                convertView = ((LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.sticky_header_item, null);
            }
            ((TextView) convertView.findViewById(R.id.textView)).setText(header);
            return convertView;
        }

        public long getHeaderId(int position) {
            Article article = (Article) getItem(position);
            Date date = new Date();
            try {
                date = new SimpleDateFormat(App.SQLITE_DATE_FORMAT, Locale.ENGLISH).parse(article.getCreateDate().substring(0, 10));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return date.getTime();
        }

        public String getHeader(int position) {
            Article article = (Article) getItem(position);
            if (!TextUtils.isEmpty(article.getCreateDate()) && article.getCreateDate().contains("/")) {
                try {
                    return Util.convertDate(App.SQLITE_DATE_FORMAT, "MM/dd", article.getCreateDate().substring(0, 10));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return "null";
        }

        public void showPhoto(SimpleLayout layoutContainer, Article article) {
            try {
                if (TextUtils.isEmpty(article.getCoverContent())) {
                    layoutContainer.setImage(BitmapFactory.decodeResource(StickyDateListFragment.this.getActivity().getResources(), R.drawable.no_image));
                } else {
                    layoutContainer.setImage(ThumbnailUtils.extractThumbnail(Util.base64ToBitmap(article.getCoverContent()), IPhotoView.DEFAULT_ZOOM_DURATION, IPhotoView.DEFAULT_ZOOM_DURATION));
                    layoutContainer.getPhotoView().setVisibility(View.VISIBLE);
                }
                layoutContainer.getPhotoView().bringToFront();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public View getView(int i, View convertView, ViewGroup viewGroup) {
            final Article article = (Article) getItem(i);
            if (convertView == null) {
                convertView = ((LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.simple_image_list_item_style_2, null);
            }
            View photoContainer = convertView.findViewById(R.id.simple_photo_container);
            ProgressBar loadingBar = (ProgressBar) convertView.findViewById(R.id.loading_bar);
            final SimpleLayout layoutContainer = LayoutFactory.getSimpleLayout(convertView, article, true);
            if (!TextUtils.isEmpty(article.getCoverContent())) {
                photoContainer.setVisibility(View.VISIBLE);
            }
            if (Util.haveNetwork(StickyDateListFragment.this.getActivity())) {
                photoContainer.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(article.getPhotoIds())) {
                    layoutContainer.getPhotoView().setVisibility(View.GONE);
                    loadingBar.setVisibility(View.GONE);
                } else if (article.getCoverContent() == null) {
                    final View finalConvertView = convertView;
                    new ServerConnector().query(ServerConnector.DOWNLOAD_THUMBNAIL_BY_ARTICLE_ID, new String[]{"articleId=" + article.getId()}, new ServerDownloadCallback() {
                        public void onDownloadFinished(Response response) {
                            if (response.getErrCode().equals(App.E000)) {
                                Photo[] photos = (Photo[]) new Gson().fromJson(response.getResponseBody(), Photo[].class);
                                if (photos == null || photos.length <= 0) {
                                    article.setCoverContent(App.noImage);
                                } else {
                                    article.setCoverContent(photos[0].getContent());
                                }
                            }
                        }

                        public void onDownloadFinishedUI(Response response) {
                            super.onDownloadFinishedUI(response);
                            if (TextUtils.isEmpty(article.getCoverContent()) || article.getCoverContent().equals(App.noImage)) {
                                finalConvertView.findViewById(R.id.simple_photo_container).setVisibility(View.GONE);
                            }
                            DateListAdapter.this.showPhoto(layoutContainer, article);
                        }
                    });
                } else {
                    showPhoto(layoutContainer, article);
                }
            } else {
                photoContainer.setVisibility(View.GONE);
            }
            layoutContainer.getLayout().setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    ArticleFragment articleFragment = new ArticleFragment();
                    articleFragment.setArticle(article);
                    ((MainActivity) StickyDateListFragment.this.getActivity()).startFragment(articleFragment);
                }
            });
            return layoutContainer.getLayout();
        }

        private String getMonthName(int month) {
            switch (month) {
                case 1:
                    return getString(R.string.jan);
                case 2:
                    return getString(R.string.feb);
                case 3:
                    return getString(R.string.mar);
                case 4:
                    return getString(R.string.apr);
                case 5:
                    return getString(R.string.may);
                case 6:
                    return getString(R.string.jun);
                case 7:
                    return getString(R.string.jul);
                case 8:
                    return getString(R.string.aug);
                case 9:
                    return getString(R.string.sep);
                case 10:
                    return getString(R.string.oct);
                case 11:
                    return getString(R.string.nov);
                default:
                    return getString(R.string.dec);
            }
        }
    }

    public StickyDateListFragment() {
        this.articles = new ArrayList();
        this.curYear = Integer.parseInt(Util.thisYear());
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.articleDao = ArticleDao.getInstance(getActivity());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_date_list_layout, null);
        this.leftBtn = this.root.findViewById(R.id.date_left);
        this.rightBtn = this.root.findViewById(R.id.date_right);
        this.listView = (StickyListHeadersListView) this.root.findViewById(R.id.news_letter_listview);
        this.yearView = (TextView) this.root.findViewById(R.id.current_year);
        this.loadingBar = (ProgressBar) this.root.findViewById(R.id.loading_bar);
        this.articles.clear();
        this.adapter = new DateListAdapter(getActivity(), this.articles);
        this.yearView.setText(String.valueOf(this.curYear));
        this.rightBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                StickyDateListFragment.this.curYear = StickyDateListFragment.this.curYear + 1;
                StickyDateListFragment.this.onYearChanged();
                StickyDateListFragment.this.validateYear(StickyDateListFragment.this.curYear);
            }
        });
        this.leftBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                StickyDateListFragment.this.curYear = StickyDateListFragment.this.curYear - 1;
                StickyDateListFragment.this.onYearChanged();
                StickyDateListFragment.this.validateYear(StickyDateListFragment.this.curYear);
            }
        });
        this.listView.setAdapter(this.adapter);
        validateYear(this.curYear);
        super.deleteRemovedData(new CustomCallback() {
            @Override
            public void onFinished() {
                StickyDateListFragment.this.checkUpdate();
            }
        });
        return this.root;
    }

    public void validateYear(int year) {
        rightBtn.setEnabled(year < Integer.parseInt(Util.thisYear()));
        leftBtn.setEnabled((year > App.minYear));
    }

    public void setArticles(List<Article> list) {
        List arrayList = list;
        if (list == null) {
            arrayList = new ArrayList();
        }
        this.articles = arrayList;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    private void checkUpdate() {
        if (Util.haveNetwork(getActivity())) {
            DownloadDataHelper.updateArticle(this.articleDao, new Callback() {
                @Override
                public void onDownloadFinished() {
                    StickyDateListFragment.this.loadData();
                }
            });
        } else {
            loadData();
        }
    }

    private void loadData() {
        List<Article> articles = this.articleDao.getArticleByCategoryIdAndYear(this.categoryId, this.curYear);
        Collections.sort(articles, new Comparator<Article>() {
            @Override
            public int compare(Article lhs, Article rhs) {
                SimpleDateFormat format = new SimpleDateFormat(StickyDateListFragment.this.getDateFormat(lhs.getCreateDate()), Locale.ENGLISH);
                try {
                    return format.parse(rhs.getCreateDate()).compareTo(format.parse(lhs.getCreateDate()));
                } catch (ParseException e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
        if (articles != null) {
            this.articles.clear();
            this.articles.addAll(articles);
        }
        this.adapter.notifyDataSetChanged();
        this.loadingBar.setVisibility(View.GONE);
    }

    private String getDateFormat(String dateStr) {
        int i;
        String[] dateStrs = dateStr.split(" ")[0].split("/");
        String dateFormat = BuildConfig.FLAVOR;
        int l = dateStrs[0].length();
        for (i = 0; i < l; i++) {
            dateFormat = dateFormat + "y";
        }
        dateFormat = dateFormat + "/";
        l = dateStrs[1].length();
        for (i = 0; i < l; i++) {
            dateFormat = dateFormat + "M";
        }
        dateFormat = dateFormat + "/";
        l = dateStrs[2].length();
        for (i = 0; i < l; i++) {
            dateFormat = dateFormat + "d";
        }
        return dateFormat;
    }

    public void onYearChanged() {
        loadData();
        this.yearView.setText(String.valueOf(this.curYear));
    }
}
