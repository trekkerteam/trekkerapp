package com.ust.trekker.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.google.gson.Gson;
import com.ust.trekker.R;
import com.ust.trekker.Dao.GameZoneQuestionDao;
import com.ust.trekker.MainActivity;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Article;
import com.ust.trekker.model.Response;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.net.ServerConnector.ServerDownloadCallback;
import com.ust.trekker.util.LayoutFactory;
import com.ust.trekker.util.LayoutFactory.SimpleLayout;
import java.util.Arrays;

public class GameZoneFragment extends DateListFragment implements GameZoneCallback {
    private GameZoneQuestionDao gameZoneQuestionDao;

    /* renamed from: com.ust.trekker.fragment.GameZoneFragment.1 */
    class C05951 implements OnClickListener {
        final /* synthetic */ Article val$article;

        C05951(Article article) {
            this.val$article = article;
        }

        public void onClick(View view) {
            QuestionFragment questionFragment = new QuestionFragment();
            questionFragment.setQuestionArticle(this.val$article);
            ((MainActivity) GameZoneFragment.this.getActivity()).startFragment(questionFragment);
        }
    }

    /* renamed from: com.ust.trekker.fragment.GameZoneFragment.2 */
    class C05962 extends ServerDownloadCallback {
        C05962() {
        }

        public void onDownloadFinished(Response response) {
            if (response.getErrCode().equals(App.E000)) {
                GameZoneFragment.this.articles.addAll(Arrays.asList((Article[]) new Gson().fromJson(response.getResponseBody(), Article[].class)));
            }
        }

        public void onDownloadFinishedUI(Response response) {
            GameZoneFragment.this.show();
        }
    }

    public GameZoneFragment() {
        this.gameZoneQuestionDao = GameZoneQuestionDao.getInstance();
    }

    public View genContentView(int i, View convertView, ViewGroup viewGroup, Object item) {
        Article article = (Article) item;
        if (convertView == null) {
            convertView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.simple_date_list_style_paper_bg, null);
        }
        SimpleLayout simpleLayout = LayoutFactory.getDateSimpleLayout(getActivity(), article);
        simpleLayout.getLayout().setOnClickListener(new C05951(article));
        return simpleLayout.getLayout();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        onStartDownloadGameQuestion();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public View genHeaderView(int position, View convertView, ViewGroup parent) {
        return null;
    }

    public void onStartDownloadGameQuestion() {
        new ServerConnector().downloadAllQuestionArticle(new C05962());
    }

    public void show() {
        notifyDataSetChange();
    }
}
