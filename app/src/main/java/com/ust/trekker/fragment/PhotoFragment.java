package com.ust.trekker.fragment;

import android.graphics.Bitmap;
import android.graphics.RectF;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import com.google.gson.Gson;
import com.ust.trekker.R;
import com.ust.trekker.Dao.PhotoAlbumDao;
import com.ust.trekker.MainActivity;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Photo;
import com.ust.trekker.model.Response;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.net.ServerConnector.ServerDownloadCallback;
import com.ust.trekker.util.Util;
import java.util.ArrayList;
import java.util.List;
import uk.co.senab.photoview.BuildConfig;
import uk.co.senab.photoview.PhotoViewAttacher;
import uk.co.senab.photoview.PhotoViewAttacher.OnMatrixChangedListener;
import uk.co.senab.photoview.PhotoViewAttacher.OnPhotoTapListener;
import uk.co.senab.photoview.PhotoViewAttacher.OnSingleFlingListener;

public class PhotoFragment extends CustomFragment {
    static final String FLING_LOG_STRING = "Fling velocityX: %.2f, velocityY: %.2f";
    static final String PHOTO_TAP_TOAST_STRING = "Photo Tap! X: %.2f %% Y:%.2f %% ID: %d";
    static final String SCALE_TOAST_STRING = "Scaled to: %.2ff";
    PhotoViewAttacher attacher;
    Bitmap bitmap;
    int curIndex;
    ImageView imageView;
    ProgressBar loadingBar;
    View nextBtn;
    Photo photo;
    PhotoAlbumDao photoAlbumDao;
    List<Integer> photoIds;
    View prevBtn;

    /* renamed from: com.ust.trekker.fragment.PhotoFragment.1 */
    class C06171 implements OnClickListener {
        C06171() {
        }

        public void onClick(View view) {
            PhotoFragment.this.changePhoto(-1);
        }
    }

    /* renamed from: com.ust.trekker.fragment.PhotoFragment.2 */
    class C06182 implements OnClickListener {
        C06182() {
        }

        public void onClick(View view) {
            PhotoFragment.this.changePhoto(1);
        }
    }

    /* renamed from: com.ust.trekker.fragment.PhotoFragment.3 */
    class C06193 extends ServerDownloadCallback {
        C06193() {
        }

        public void onDownloadFinished(Response response) {
            if (response.getErrCode().equals(App.E000)) {
                PhotoFragment.this.photo.clone((Photo) new Gson().fromJson(response.getResponseBody(), Photo.class));
                PhotoFragment.this.photoAlbumDao.addPhoto(PhotoFragment.this.photo);
            }
        }

        public void onDownloadFinishedUI(Response response) {
            PhotoFragment.this.refreshPhoto();
        }
    }

    private class MatrixChangeListener implements OnMatrixChangedListener {
        private MatrixChangeListener() {
        }

        public void onMatrixChanged(RectF rect) {
            LayoutParams params = PhotoFragment.this.imageView.getLayoutParams();
            params.height = ((View) PhotoFragment.this.imageView.getParent()).getWidth();
            PhotoFragment.this.imageView.setLayoutParams(params);
        }
    }

    private class PhotoTapListener implements OnPhotoTapListener {
        private PhotoTapListener() {
        }

        public void onPhotoTap(View view, float x, float y) {
            float xPercentage = x * 100.0f;
            float yPercentage = y * 100.0f;
        }

        public void onOutsidePhotoTap() {
        }
    }

    private class SingleFlingListener implements PhotoViewAttacher.OnSingleFlingListener {
        private SingleFlingListener() {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return true;
        }
    }

    public PhotoFragment() {
        this.photoAlbumDao = PhotoAlbumDao.getInstance();
        this.photoIds = new ArrayList();
        this.curIndex = -1;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.bitmap = Util.base64ToBitmap(this.photo.getContent());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRoot(inflater, R.layout.fragment_photo, null);
        this.imageView = (ImageView) this.root.findViewById(R.id.fragment_photo_view);
        this.loadingBar = (ProgressBar) this.root.findViewById(R.id.loading_bar);
        this.prevBtn = this.root.findViewById(R.id.prev_photo);
        this.nextBtn = this.root.findViewById(R.id.next_photo);
        this.prevBtn.setOnClickListener(new C06171());
        this.nextBtn.setOnClickListener(new C06182());
        if (this.curIndex >= this.photoIds.size() - 1) {
            this.nextBtn.setVisibility(View.GONE);
        }
        if (this.curIndex <= 0) {
            this.prevBtn.setVisibility(View.GONE);
        }
        loadData();
        return this.root;
    }

    private void changePhoto(int dir) {
        if (this.curIndex + dir <= this.photoIds.size() - 1 && this.curIndex + dir >= 0) {
            Photo photo = this.photoAlbumDao.getPhotoByPhotoId(((Integer) this.photoIds.get(this.curIndex + dir)).intValue());
            if (photo == null) {
                photo = new Photo();
                photo.setId(((Integer) this.photoIds.get(this.curIndex + dir)).intValue());
                photo.setName(BuildConfig.FLAVOR);
            }
            PhotoFragment photoFragment = new PhotoFragment();
            photoFragment.setPhoto(photo);
            photoFragment.setPhotoIds(this.photoIds);
            ((MainActivity) getActivity()).switchFragment(photoFragment);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.attacher != null) {
            this.attacher.cleanup();
        }
    }

    public void loadData() {
        Photo tmp = this.photoAlbumDao.getPhotoByPhotoId(this.photo.getId());
        if (tmp == null || TextUtils.isEmpty(tmp.getContent())) {
            new ServerConnector().downloadPhotoByPhotoId(BuildConfig.FLAVOR + this.photo.getId(), new C06193());
            return;
        }
        this.photo = tmp;
        refreshPhoto();
    }

    private void refreshPhoto() {
        bitmap = Util.base64ToBitmap(this.photo.getContent());
        imageView.setImageBitmap(this.bitmap);
        attacher = new PhotoViewAttacher(this.imageView);
        attacher.setOnMatrixChangeListener(new MatrixChangeListener());
        attacher.setOnPhotoTapListener(new PhotoTapListener());
        attacher.setOnSingleFlingListener(new SingleFlingListener());
        attacher.setScaleType(ScaleType.FIT_CENTER);
        loadingBar.setVisibility(View.GONE);
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
        setTitle(photo.getName());
    }

    public void setPhotoIds(List<Integer> photoIds) {
        this.photoIds.addAll(photoIds);
        if (this.photo != null) {
            for (int i = 0; i < photoIds.size(); i++) {
                if (this.photo.getId() == ((Integer) photoIds.get(i)).intValue()) {
                    this.curIndex = i;
                    return;
                }
            }
        }
    }
}
