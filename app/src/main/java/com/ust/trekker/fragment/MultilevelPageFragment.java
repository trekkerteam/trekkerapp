package com.ust.trekker.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ust.trekker.R;
import com.ust.trekker.model.Category;
import java.util.ArrayList;
import java.util.List;

public abstract class MultilevelPageFragment extends CustomFragment {
    protected ArrayAdapter<Category> adapter;
    protected List<Category> categories;
    protected ListView listView;
    protected ProgressBar loadingBar;
    protected View root;

    private class MultilevelAdapter extends ArrayAdapter<Category> {

      
        public MultilevelAdapter() {
            super(MultilevelPageFragment.this.getActivity(), -1, MultilevelPageFragment.this.categories);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.mulitlevel_list_item, null);
            }
            final Category category = (Category) getItem(position);
            ((TextView) convertView.findViewById(R.id.textView)).setText(category.getCategorName());
            convertView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    MultilevelPageFragment.this.onItemClicked(category);
                }
            });
            return convertView;
        }
    }

    public abstract void onItemClicked(Category category);

    public MultilevelPageFragment() {
        this.categories = new ArrayList();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.adapter = new MultilevelAdapter();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_multilevel_layout, null);
        this.listView = (ListView) this.root.findViewById(R.id.multilevel_listview);
        this.loadingBar = (ProgressBar) this.root.findViewById(R.id.loading_bar);
        this.listView.setAdapter(this.adapter);
        return this.root;
    }

    protected void notifyDataSetChanged() {
        this.loadingBar.setVisibility(8);
        this.adapter.notifyDataSetChanged();
    }

    public void setCategories(List<Category> categories) {
        this.categories.clear();
        this.categories.addAll(categories);
    }
}
