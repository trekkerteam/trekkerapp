package com.ust.trekker.fragment;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.ust.trekker.R;
import com.ust.trekker.Dao.ArticleDao;
import com.ust.trekker.Dao.CategoryDao;
import com.ust.trekker.MainActivity;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Article;
import com.ust.trekker.model.Photo;
import com.ust.trekker.model.Response;
import com.ust.trekker.net.DownloadDataHelper;
import com.ust.trekker.net.DownloadDataHelper.Callback;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.net.ServerConnector.ServerDownloadCallback;
import com.ust.trekker.util.Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.lucasr.twowayview.TwoWayView;
import uk.co.senab.photoview.IPhotoView;

public class HomeFragment extends CustomFragment implements HomeFragmentInterface {
    ArticleDao articleDao;
    CategoryDao categoryDao;
    List<Article> eventNews;
    ArrayAdapter<Article> eventNewsAdapter;
    TwoWayView eventNewsTwoWayView;
    ArrayAdapter<Article> newLetersAdapter;
    TwoWayView newsLetterTwoWayView;
    List<Article> newsLetters;
    View root;
    
    private class LatestArticleAdapter extends ArrayAdapter<Article> {


        public LatestArticleAdapter(Context context, List<Article> articles) {
            super(context, -1, articles);
        }

        public void showPhoto(ImageView photoView, Article article) {
            if (photoView != null) {
                try {
                    if (TextUtils.isEmpty(article.getCoverContent())) {
                        photoView.setImageBitmap(BitmapFactory.decodeResource(HomeFragment.this.getActivity().getResources(), R.drawable.no_image));
                    } else {
                        photoView.setImageBitmap(ThumbnailUtils.extractThumbnail(Util.base64ToBitmap(article.getCoverContent()), IPhotoView.DEFAULT_ZOOM_DURATION, IPhotoView.DEFAULT_ZOOM_DURATION));
                    }
                    photoView.bringToFront();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final Article newsLetter = (Article) getItem(position);
            View itemLayout = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.simple_image_list_item_style_1, null);
            TextView titleView = (TextView) itemLayout.findViewById(R.id.simple_title);
            TextView descView = (TextView) itemLayout.findViewById(R.id.simple_desc);
            final ImageView imageView = (ImageView) itemLayout.findViewById(R.id.simple_image);
            if (newsLetter.getCoverContent() == null) {
                ServerConnector con = new ServerConnector();
                try {
                    con.query(ServerConnector.DOWNLOAD_THUMBNAIL_BY_ARTICLE_ID, new String[]{"articleId=" + newsLetter.getId()}, new ServerDownloadCallback() {
                        Photo[] photos;
                        @Override
                        public void onDownloadFinished(Response response) {
                            if (response.getErrCode().equals(App.E000)) {
                                this.photos = (Photo[]) new Gson().fromJson(response.getResponseBody(), Photo[].class);
                                if (this.photos != null && this.photos.length > 0) {
                                    newsLetter.setCoverContent(this.photos[0].getContent());
                                }
                            }
                        }
                        
                        @Override
                        public void onDownloadFinishedUI(Response response) {
                            super.onDownloadFinishedUI(response);
                            LatestArticleAdapter.this.showPhoto(imageView, newsLetter);
                        }
                    });
                } catch (Exception ioe) {
                    ioe.printStackTrace();
                }
            } else {
                showPhoto(imageView, newsLetter);
            }
            titleView.setText(newsLetter.getTitle());
            descView.setText(newsLetter.getContent());
            itemLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    ArticleFragment articleFragment = new ArticleFragment();
                    articleFragment.setArticle(newsLetter);
                    ((MainActivity) HomeFragment.this.getActivity()).startFragment(articleFragment);
                }
            });
            return itemLayout;
        }
    }

    public HomeFragment() {
        this.newsLetters = new ArrayList();
        this.eventNews = new ArrayList();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.categoryDao = CategoryDao.getInstance(getActivity());
        this.articleDao = ArticleDao.getInstance(getActivity());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_main_layout, container, false);
        this.newsLetterTwoWayView = (TwoWayView) this.root.findViewById(R.id.fragment_main_listview_h1);
        this.eventNewsTwoWayView = (TwoWayView) this.root.findViewById(R.id.fragment_main_listview_h2);
        this.newLetersAdapter = new LatestArticleAdapter(getActivity(), this.newsLetters);
        this.eventNewsAdapter = new LatestArticleAdapter(getActivity(), this.eventNews);
        this.newsLetterTwoWayView.setAdapter(this.newLetersAdapter);
        this.eventNewsTwoWayView.setAdapter(this.eventNewsAdapter);
        return this.root;
    }

    public void onResume() {
        super.onResume();
        checkUpdate();
        loadData();
    }

    public void checkUpdate() {
        DownloadDataHelper.updateArticle(this.articleDao, new Callback() {
            @Override
            public void onDownloadFinished() {
                HomeFragment.this.loadData();
            }
        });
    }

    public void loadData() {
        List<Article> newsLetters = this.articleDao.getArticleByCategoryIdCascade(App.NEWS_ID, this.categoryDao);
        List<Article> eventNews = this.articleDao.getArticleByCategoryId(App.EVEN_ID);
        this.newsLetters.clear();
        if (newsLetters != null && newsLetters.size() > 0) {
            for (Article article : newsLetters) {
                if (this.newsLetters.size() > 3) {
                    break;
                }
                this.newsLetters.add(article);
            }
        }
        this.eventNews.clear();
        if (eventNews != null && eventNews.size() > 0) {
            for (Article article2 : eventNews) {
                if (this.eventNews.size() > 3) {
                    break;
                }
                this.eventNews.add(article2);
            }
        }
        this.newLetersAdapter.notifyDataSetChanged();
        this.eventNewsAdapter.notifyDataSetChanged();
    }

    public void onDownloadNewsLetterUpdates() {
        try {
            new ServerConnector().downloadLatestAritcleByCatid(App.NEWS_ID, new ServerDownloadCallback() {
                @Override
                public void onDownloadFinished(Response response) {
                    if (response.ErrCode.equals(App.E000)) {
                        HomeFragment.this.newsLetters.clear();
                        HomeFragment.this.newsLetters.addAll(Arrays.asList((Article[]) new Gson().fromJson(response.getResponseBody(), Article[].class)));
                    }
                }

                @Override
                public void onDownloadFinishedUI(Response response) {
                    HomeFragment.this.onDownloadEventNewsUpdates();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            onDownloadEventNewsUpdates();
        }
    }

    public void onDownloadEventNewsUpdates() {
        try {
            new ServerConnector().downloadLatestAritcleByCatid(App.EVEN_ID, new ServerDownloadCallback() {
                @Override
                public void onDownloadFinished(Response response) {
                    if (response.ErrCode.equals(App.E000)) {
                        HomeFragment.this.eventNews.clear();
                        HomeFragment.this.eventNews.addAll(Arrays.asList((Article[]) new Gson().fromJson(response.getResponseBody(), Article[].class)));
                    }
                }

                @Override
                public void onDownloadFinishedUI(Response response) {
                    HomeFragment.this.show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            show();
        }
    }

    public void show() {
        this.newsLetterTwoWayView.setAdapter(this.newLetersAdapter);
        this.eventNewsTwoWayView.setAdapter(this.eventNewsAdapter);
    }
}
