package com.ust.trekker.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.ust.trekker.Dao.ArticleDao;
import com.ust.trekker.Dao.CategoryDao;
import com.ust.trekker.Dao.Dao;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Response;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.net.ServerConnector.ServerDownloadCallback;
import com.ust.trekker.util.Util;

public abstract class CustomFragment extends Fragment {
    ArticleDao articleDao;
    CategoryDao categoryDao;
    public int index;
    protected ProgressBar loadingBar;
    View root;
    public String title;

    protected interface CustomCallback {
        void onFinished();
    }

    public CustomFragment() {
        this.articleDao = ArticleDao.getInstance();
        this.categoryDao = CategoryDao.getInstance();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onDestroyView() {
        super.onDestroyView();
        ServerConnector.cancelAll();
    }

    protected void setRoot(LayoutInflater inflater, int resource, ViewGroup parent) {
        this.root = inflater.inflate(resource, parent);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    protected void deleteRemovedData(CustomCallback callback) {
        if (Util.haveNetwork(getActivity())) {
            deleteRemovedData("article", this.articleDao, false, callback);
        } else {
            callback.onFinished();
        }
    }

    protected void deleteRemovedData(String tableName, final Dao dao, final boolean stop, final CustomCallback callback) {
        new ServerConnector().getDeletedItem(tableName, dao.getAllServerId(), new ServerDownloadCallback() {
            public void onDownloadFinished(Response response) {
                if (response.getErrCode().equals(App.E000)) {
                    dao.deleteByServerId(response.getResponseBody());
                    if (stop) {
                        callback.onFinished();
                        return;
                    } else {
                        CustomFragment.this.deleteRemovedData("category", CustomFragment.this.categoryDao, true, callback);
                        return;
                    }
                }
                callback.onFinished();
            }
        });
    }

    protected void onDownloadData() {
    }

    protected void onDataDownloaded() {
        if (this.loadingBar != null) {
            this.loadingBar.setVisibility(8);
        }
    }
}
