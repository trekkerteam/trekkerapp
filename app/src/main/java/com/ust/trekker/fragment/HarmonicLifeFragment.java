package com.ust.trekker.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ust.trekker.Dao.ArticleDao;
import com.ust.trekker.Dao.CategoryDao;
import com.ust.trekker.MainActivity;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Category;
import com.ust.trekker.net.DownloadDataHelper;
import com.ust.trekker.net.DownloadDataHelper.Callback;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.util.Util;
import java.util.List;

public class HarmonicLifeFragment extends MultilevelPageFragment {
    ArticleDao articleDao;
    CategoryDao categoryDao;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.categoryDao = CategoryDao.getInstance(getActivity());
        this.articleDao = ArticleDao.getInstance(getActivity());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        super.deleteRemovedData(new CustomCallback() {
            public void onFinished() {
                HarmonicLifeFragment.this.checkUpdate();
            }
        });
        sendStatistic();
        return this.root;
    }

    public void checkUpdate() {
        if (Util.haveNetwork(getActivity())) {
            DownloadDataHelper.updateCategory(this.categoryDao, new Callback() {
                @Override
                public void onDownloadFinished() {
                    HarmonicLifeFragment.this.loadData();
                }

            });
        } else {
            loadData();
        }
    }

    public void loadData() {
        List<Category> categories = this.categoryDao.getCategoryByParentCatIdAndYear(App.HARM_ID, "20");
        if (categories != null && categories.size() > 0) {
            setCategories(categories);
            notifyDataSetChanged();
        }
    }

    public void onItemClicked(Category category) {
        StickyDateListFragment stickyDateListFragment = new StickyDateListFragment();
        stickyDateListFragment.setArticles(this.articleDao.getArticleByCategoryId(category.getId()));
        stickyDateListFragment.setCategoryId(category.getId());
        stickyDateListFragment.setTitle(category.getCategorName());
        ((MainActivity) getActivity()).startFragment(stickyDateListFragment);
    }

    public void sendStatistic() {
        new ServerConnector().sendStatic(App.cred, String.valueOf(App.HARM_ID));
    }
}
