package com.ust.trekker.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ProgressBar;
import com.google.gson.Gson;
import com.ust.trekker.R;
import com.ust.trekker.Dao.PhotoAlbumDao;
import com.ust.trekker.MainActivity;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Article;
import com.ust.trekker.model.Photo;
import com.ust.trekker.model.PhotoAlbum;
import com.ust.trekker.model.Response;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.net.ServerConnector.ServerDownloadCallback;
import com.ust.trekker.util.LayoutFactory;
import com.ust.trekker.util.LayoutFactory.SimpleLayout;
import java.util.ArrayList;
import java.util.List;

public class PhotoAlbumContentFragment extends CustomFragment {
    ImageAdapter adapter;
    GridView gridView;
    ProgressBar loadingBar;
    PhotoAlbum photoAlbum;
    PhotoAlbumDao photoAlbumDao;
    List<Integer> photoIds;
    List<Photo> photos;
    View root;

    public class ImageAdapter extends BaseAdapter {
        private Context context;
        List<Photo> photos;

        public ImageAdapter(Context context, List<Photo> photos) {
            this.context = context;
            this.photos = photos;
        }

        public int getCount() {
            return this.photos.size();
        }

        public Photo getItem(int i) {
            return (Photo) this.photos.get(i);
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View convertView, ViewGroup viewGroup) {
            final Photo photo = getItem(i);
            if (convertView == null) {
                convertView = ((LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.simple_image_list_item_style_1, null);
            }
            final SimpleLayout simpleLayout = LayoutFactory.getSimpleLayout(convertView, new Article(), true);
            if (TextUtils.isEmpty(photo.getContent())) {
                new ServerConnector().query(ServerConnector.DOWNLOAD_THUMBNAIL_BY_PHOTO_ID, new String[]{"photoId=" + photo.getId()}, new ServerDownloadCallback() {
                    @Override
                    public void onDownloadFinished(Response response) {
                    }

                    @Override
                    public void onDownloadFinishedUI(Response response) {
                        if (response.getErrCode().equals(App.E000)) {
                            photo.clone((Photo) new Gson().fromJson(response.getResponseBody(), Photo.class));
                            ImageAdapter.this.showImage(photo, simpleLayout);
                        }
                    }
                });
            } else {
                showImage(photo, simpleLayout);
            }
            return simpleLayout.getLayout();
        }

        public void showImage(final Photo photo, SimpleLayout simpleLayout) {
            simpleLayout.setImage(photo.getContent());
            simpleLayout.setTitle(photo.getName());
            simpleLayout.getLayout().setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    PhotoFragment photoFragment = new PhotoFragment();
                    photoFragment.setPhoto(photo);
                    photoFragment.setPhotoIds(PhotoAlbumContentFragment.this.getPhotoIds(PhotoAlbumContentFragment.this.photos));
                    ((MainActivity) PhotoAlbumContentFragment.this.getActivity()).startFragment(photoFragment);
                }
            });
        }
    }

    public PhotoAlbumContentFragment() {
        this.photos = new ArrayList();
        this.photoAlbumDao = PhotoAlbumDao.getInstance();
        this.photoIds = new ArrayList();
    }

    public void onCreate(Bundle savedInstanceState) {
        if (this.photoAlbum == null) {
            getActivity().onBackPressed();
        }
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_photo_album_content_list, null);
        this.gridView = (GridView) this.root.findViewById(R.id.photo_album_content);
        this.loadingBar = (ProgressBar) this.root.findViewById(R.id.loading_bar);
        this.adapter = new ImageAdapter(getActivity(), this.photos);
        loadPhotos();
        this.loadingBar.setVisibility(8);
        this.gridView.setAdapter(this.adapter);
        return this.root;
    }

    public void loadPhotos() {
        List<Photo> cachedPhotos = this.photoAlbumDao.getPhotoByAlbumId(this.photoAlbum.getId());
        if (cachedPhotos != null && cachedPhotos.size() > 0) {
            this.photos.clear();
            this.photos.addAll(cachedPhotos);
        } else if (!TextUtils.isEmpty(this.photoAlbum.getPhotoIds())) {
            for (String id : this.photoAlbum.getPhotoIds().split(",")) {
                Photo photo = new Photo();
                photo.setId(Integer.parseInt(id));
                photo.setAlbumId(this.photoAlbum.getId());
                this.photoAlbumDao.addPhotoToAlbum(photo);
                this.photos.add(photo);
            }
        }
    }

    public void setAlbum(PhotoAlbum photoAlbum) {
        this.photoAlbum = photoAlbum;
        setTitle(photoAlbum.getName());
    }

    private List<Integer> getPhotoIds(List<Photo> photos) {
        List<Integer> ids = new ArrayList();
        for (Photo photo : photos) {
            ids.add(Integer.valueOf(photo.getId()));
        }
        return ids;
    }
}
