package com.ust.trekker.fragment;

/* compiled from: NewsLetterFragment */
interface NewsLetterFragmentCallback {
    void onUpdateCategory();

    void onYearChanged();
}
