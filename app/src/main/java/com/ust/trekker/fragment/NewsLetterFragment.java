package com.ust.trekker.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ust.trekker.R;
import com.ust.trekker.Dao.ArticleDao;
import com.ust.trekker.Dao.CategoryDao;
import com.ust.trekker.MainActivity;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Article;
import com.ust.trekker.model.Category;
import com.ust.trekker.net.DownloadDataHelper;
import com.ust.trekker.net.DownloadDataHelper.Callback;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.util.Util;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewsLetterFragment extends CustomFragment implements NewsLetterFragmentCallback {
    ExpandableListAdapter adapter;
    ArticleDao articleDao;
    ExpandableListView categoriesView;
    CategoryDao categoryDao;
    Map<Integer, List<Category>> child;
    String curYear;
    List<Category> header;
    View leftBtn;
    ProgressBar progressBar;
    View rightBtn;
    View root;
    TextView yearView;
   
  
    private class ExpandableListAdapter extends BaseExpandableListAdapter {
        private Context context;
        private Map<Integer, List<Category>> dataChild;
        private List<Category> dataHeader;


        public ExpandableListAdapter(Context context, List<Category> dataHeader, Map<Integer, List<Category>> dataChild) {
            this.context = context;
            this.dataHeader = dataHeader;
            this.dataChild = dataChild;
        }

        public int getGroupCount() {
            return this.dataHeader.size();
        }

        public int getChildrenCount(int i) {
            try {
                return ((List) this.dataChild.get(Integer.valueOf(((Category) this.dataHeader.get(i)).getId()))).size();
            } catch (Exception e) {
                return 0;
            }
        }

        public Object getGroup(int i) {
            return this.dataHeader.get(i);
        }

        public Object getChild(int i, int i1) {
            return ((List) this.dataChild.get(Integer.valueOf(((Category) this.dataHeader.get(i)).getId()))).get(i1);
        }

        public long getGroupId(int i) {
            return (long) i;
        }

        public long getChildId(int i, int i1) {
            return (long) i1;
        }

        public boolean hasStableIds() {
            return false;
        }

        public View getGroupView(int i, boolean b, View convertView, ViewGroup viewGroup) {
            if (convertView == null) {
                convertView = ((LayoutInflater) NewsLetterFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.expandable_list_header_item, null);
            }
            ((TextView) convertView.findViewById(R.id.header_textview)).setText(((Category) getGroup(i)).getCategorName());
            return convertView;
        }

        public View getChildView(int i, int i1, boolean b, View convertView, ViewGroup viewGroup) {
            if (convertView == null) {
                convertView = ((LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.simple_text_view, null);
            }
            final Category category = (Category) getChild(i, i1);
            ((TextView) convertView).setText(category.getCategorName());
            convertView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<Article> articles = NewsLetterFragment.this.articleDao.getArticleByCategoryId(category.getId());
                    if (articles == null || articles.size() == 0) {
                        articles = new ArrayList();
                        Article tmpArticle = new Article();
                        tmpArticle.setCategoryId(category.getId());
                        articles.add(tmpArticle);
                    }
                    ArticleFragment articleFragment = new ArticleFragment();
                    articleFragment.setArticle((Article) articles.get(0));
                    ((MainActivity) NewsLetterFragment.this.getActivity()).startFragment(articleFragment);
                    new ServerConnector().sendStatic(App.cred, String.valueOf(category.getId()));
                }
            });
            return convertView;
        }

        public boolean isChildSelectable(int i, int i1) {
            return true;
        }
    }

    public NewsLetterFragment() {
        this.header = new ArrayList();
        this.child = new HashMap();
        this.curYear = Util.thisYear();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.categoryDao = CategoryDao.getInstance(getActivity());
        this.articleDao = ArticleDao.getInstance();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_news_letter_layout, null);
        this.categoriesView = (ExpandableListView) this.root.findViewById(R.id.news_letter_listView);
        this.yearView = (TextView) this.root.findViewById(R.id.current_year);
        this.leftBtn = this.root.findViewById(R.id.date_left);
        this.rightBtn = this.root.findViewById(R.id.date_right);
        this.progressBar = (ProgressBar) this.root.findViewById(R.id.loading_bar);
        this.yearView.setText(this.curYear);
        this.leftBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                NewsLetterFragment.this.curYear = String.valueOf(Integer.parseInt(NewsLetterFragment.this.curYear) - 1);
                NewsLetterFragment.this.onYearChanged();
                NewsLetterFragment.this.validateYear(Integer.parseInt(NewsLetterFragment.this.curYear));
                NewsLetterFragment.this.yearView.setText(String.valueOf(NewsLetterFragment.this.curYear));
            }
        });
        this.rightBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                NewsLetterFragment.this.curYear = String.valueOf(Integer.parseInt(NewsLetterFragment.this.curYear) + 1);
                NewsLetterFragment.this.onYearChanged();
                NewsLetterFragment.this.validateYear(Integer.parseInt(NewsLetterFragment.this.curYear));
                NewsLetterFragment.this.yearView.setText(String.valueOf(NewsLetterFragment.this.curYear));
            }
        });
        this.adapter = new ExpandableListAdapter(getActivity(), this.header, this.child);
        this.categoriesView.setAdapter(this.adapter);
        validateYear(Integer.parseInt(this.curYear));
        super.deleteRemovedData(new CustomCallback() {
            @Override
            public void onFinished() {
                NewsLetterFragment.this.checkUpdate();
            }
        });
        sendStatic();
        return this.root;
    }

    public void validateYear(int year) {
        boolean z;
        boolean z2 = true;
        View view = this.rightBtn;
        if (year < Integer.parseInt(Util.thisYear())) {
            z = true;
        } else {
            z = false;
        }
        view.setEnabled(z);
        View view2 = this.leftBtn;
        if (year <= App.minYear) {
            z2 = false;
        }
        view2.setEnabled(z2);
    }

    private void checkUpdate() {
        if (Util.haveNetwork(getActivity())) {
            DownloadDataHelper.updateCategory(this.categoryDao, new Callback() {
                @Override
                public void onDownloadFinished() {
                    NewsLetterFragment.this.loadData();
                }
            });
        } else {
            loadData();
        }
    }

    private void loadData() {
        List<Category> header = this.categoryDao.getCategoryByParentCatIdAndYear(App.NEWS_ID, this.curYear);
        this.header.clear();
        this.child.clear();
        if (header != null && header.size() > 0) {
            this.header.addAll(header);
            for (Category category : header) {
                this.child.put(Integer.valueOf(category.getId()), this.categoryDao.getCategoryByParentCatIdReverse(category.getId()));
            }
        }
        notifyDataSetChange();
    }

    private void loadData(Category[] categories) {
        this.header.clear();
        this.child.clear();
        for (Category c : categories) {
            if (c.getParentCatId() == App.NEWS_ID) {
                this.header.add(c);
            } else {
                List<Category> tmpList = (List) this.child.get(Integer.valueOf(c.getParentCatId()));
                if (tmpList == null) {
                    tmpList = new ArrayList();
                }
                tmpList.add(c);
                this.child.put(Integer.valueOf(c.getParentCatId()), tmpList);
            }
        }
    }

    public void onYearChanged() {
        loadData();
    }

    public void notifyDataSetChange() {
        if (this.adapter != null) {
            this.adapter.notifyDataSetChanged();
        }
        if (this.progressBar != null) {
            this.progressBar.setVisibility(8);
        }
    }

    public void sendStatic() {
        new ServerConnector().sendStatic(App.cred, String.valueOf(App.NEWS_ID));
    }

    public void onUpdateCategory() {
    }
}
