package com.ust.trekker.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import com.ust.trekker.R;
import com.ust.trekker.model.Article;
import com.ust.trekker.model.GameZoneQuestion;
import com.ust.trekker.model.Response;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.net.ServerConnector.ServerDownloadCallback;
import com.ust.trekker.util.AnimationFactory;
import com.ust.trekker.util.LayoutFactory;
import com.ust.trekker.util.MockData;
import com.ust.trekker.view.AnimatingRelativeLayout;
import java.util.List;

public class QuestionFragment extends CustomFragment implements QuestionFragemntCallback {
    FloatingActionButton fab;
    Article questionArticle;
    AnimatingRelativeLayout questionView;
    View root;

    /* renamed from: com.ust.trekker.fragment.QuestionFragment.1 */
    class C06201 implements OnClickListener {
        C06201() {
        }

        public void onClick(View view) {
            QuestionFragment.this.questionView.show();
            AnimationFactory.fadeOut(QuestionFragment.this.fab, 100);
        }
    }

    /* renamed from: com.ust.trekker.fragment.QuestionFragment.2 */
    class C06212 implements OnClickListener {
        C06212() {
        }

        public void onClick(View view) {
            QuestionFragment.this.questionView.hide();
            AnimationFactory.fadeIn(QuestionFragment.this.fab, 100);
        }
    }

    /* renamed from: com.ust.trekker.fragment.QuestionFragment.3 */
    class C06223 extends ServerDownloadCallback {
        C06223() {
        }

        public void onDownloadFinished(Response response) {
        }
    }

    /* renamed from: com.ust.trekker.fragment.QuestionFragment.4 */
    class C06234 extends ServerDownloadCallback {
        C06234() {
        }

        public void onDownloadFinished(Response response) {
        }
    }

    private class QuestionAdapter extends ArrayAdapter<GameZoneQuestion> {
        Context context;

        /* renamed from: com.ust.trekker.fragment.QuestionFragment.QuestionAdapter.1 */
        class C06241 implements OnClickListener {
            C06241() {
            }

            public void onClick(View view) {
                QuestionFragment.this.questionView.hide();
                AnimationFactory.fadeIn(QuestionFragment.this.fab, 100);
            }
        }

        public QuestionAdapter(Context context, List<GameZoneQuestion> list) {
            super(context, -1, list);
            this.context = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            GameZoneQuestion question = (GameZoneQuestion) getItem(position);
            if (position != getCount() - 1) {
                return LayoutFactory.getQuestionLayout(this.context, null, position + 1, question);
            }
            View view = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.enter_button, null);
            view.setOnClickListener(new C06241());
            return view;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_question_layout, container, false);
        ScrollView questionContainer = (ScrollView) this.root.findViewById(R.id.game_zone_question_container);
        ListView questionListView = (ListView) this.root.findViewById(R.id.game_zone_answer_listVIew);
        View hideQuestionContainer = this.root.findViewById(R.id.game_zone_answer_hide_view);
        this.questionView = (AnimatingRelativeLayout) this.root.findViewById(R.id.game_zone_answer_container);
        this.fab = (FloatingActionButton) this.root.findViewById(R.id.fab_answer_question);
        if (this.questionArticle != null) {
            questionContainer.addView(LayoutFactory.getSimpleArticle1(getActivity(), this.questionArticle));
        }
        this.fab.setOnClickListener(new C06201());
        hideQuestionContainer.setOnClickListener(new C06212());
        questionListView.setAdapter(new QuestionAdapter(getActivity(), MockData.getQuestion()));
        return this.root;
    }

    public void setQuestionArticle(Article questionArticle) {
        this.questionArticle = questionArticle;
    }

    public void onDownloadArticlePhotos() {
        new ServerConnector().downloadQuestionByArticleId(this.questionArticle.getId(), new C06223());
    }

    public void onDownloadArticleQuestion() {
        new ServerConnector().downloadQuestionByArticleId(this.questionArticle.getId(), new C06234());
    }
}
