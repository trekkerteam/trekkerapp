package com.ust.trekker.fragment;

import android.content.Context;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.gson.Gson;
import com.ust.trekker.R;
import com.ust.trekker.Dao.PhotoAlbumDao;
import com.ust.trekker.MainActivity;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Article;
import com.ust.trekker.model.Photo;
import com.ust.trekker.model.Response;
import com.ust.trekker.net.ServerConnector;
import com.ust.trekker.net.ServerConnector.ServerDownloadCallback;
import com.ust.trekker.util.Util;
import java.util.ArrayList;
import java.util.List;
import org.lucasr.twowayview.TwoWayView;

public class ArticleFragment extends CustomFragment {
    Article article;
    TextView contentView;
    TextView dateView;
    View leftBtn;
    ProgressBar loadingBar;
    TextView newsDateView;
    View newsSourceContainer;
    TextView newsSourceView;
    PhotoAlbumDao photoAlbumDao;
    View photoContainer;
    List<Photo> photos;
    View rightBtn;
    View root;
    ArrayAdapter<Photo> thumbnailAdapter;
    TextView titleView;
    TwoWayView twoWayView;


    public ArticleFragment() {
        this.photoAlbumDao = PhotoAlbumDao.getInstance();
        this.photos = new ArrayList();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.simple_article_layout, null);
        this.titleView = (TextView) this.root.findViewById(R.id.simple_article_title);
        this.dateView = (TextView) this.root.findViewById(R.id.simple_article_date);
        this.contentView = (TextView) this.root.findViewById(R.id.simple_article_content);
        this.photoContainer = this.root.findViewById(R.id.article_photo_container);
        this.twoWayView = (TwoWayView) this.root.findViewById(R.id.simple_article_image_list);
        this.leftBtn = this.root.findViewById(R.id.simple_article_left);
        this.rightBtn = this.root.findViewById(R.id.simple_article_right);
        this.newsSourceView = (TextView) this.root.findViewById(R.id.simple_news_source);
        this.newsDateView = (TextView) this.root.findViewById(R.id.simple_news_date);
        this.newsSourceContainer = this.root.findViewById(R.id.article_news_source_container);
        this.thumbnailAdapter = new ArrayAdapter<Photo>(getActivity(), R.layout.simple_image_list_item_style_1, this.photos) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View itemLayout;
                int i;
                if (convertView != null) {
                    itemLayout = convertView;
                } else {
                    itemLayout = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.simple_image_layout, null);
                }
                final ProgressBar loadingBar = (ProgressBar) itemLayout.findViewById(R.id.loading_bar);
                final ImageView imageView = (ImageView) itemLayout.findViewById(R.id.simple_image);
                final Photo photo = (Photo) getItem(position);
                View view = ArticleFragment.this.rightBtn;
                if (position == ArticleFragment.this.photos.size() - 1) {
                    i = 8;
                } else {
                    i = 0;
                }
                view.setVisibility(i);
                view = ArticleFragment.this.leftBtn;
                if (position == 0) {
                    i = 8;
                } else {
                    i = 0;
                }
                view.setVisibility(i);
                imageView.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        PhotoFragment photoFragment = new PhotoFragment();
                        photoFragment.setPhoto(photo);
                        photoFragment.setPhotoIds(ArticleFragment.this.getArticlePhotoIds(ArticleFragment.this.photos));
                        ((MainActivity) ArticleFragment.this.getActivity()).startFragment(photoFragment);
                    }
                });
                if (TextUtils.isEmpty(photo.getContent()) && Util.haveNetwork(ArticleFragment.this.getActivity())) {
                    new ServerConnector().query(ServerConnector.DOWNLOAD_THUMBNAIL_BY_PHOTO_ID, new String[]{"photoId=" + photo.getId()}, new ServerDownloadCallback() {
                        public void onDownloadFinished(Response response) {
                            if (response.ErrCode.equals(App.E000)) {
                                Photo p = (Photo) new Gson().fromJson(response.getResponseBody(), Photo.class);
                                ArticleFragment.this.photoAlbumDao.addArticlePhoto(p);
                                photo.clone(p);
                            }
                        }

                        public void onDownloadFinishedUI(Response response) {
                            if (loadingBar != null) {
                                loadingBar.setVisibility(View.GONE);
                            }
                            refresh(photo, imageView);
                        }
                    });
                } else {
                    if (loadingBar != null) {
                        loadingBar.setVisibility(8);
                    }
                    refresh(photo, imageView);
                }
                return itemLayout;
            }

        };
        if (!Util.haveNetwork(getActivity())) {
            this.photoContainer.setVisibility(View.GONE);
        }
        if (this.article.getId() == -1) {
            downloadArticle();
        } else {
            init();
            loadData();
        }
        return this.root;
    }

    public void onPause() {
        super.onPause();
    }

    public void init() {
        this.twoWayView.setAdapter(this.thumbnailAdapter);
        if (this.article != null) {
            this.titleView.setText(this.article.getTitle());
            TextView textView = this.dateView;
            CharSequence createDate = (this.article.getCreateDate() == null || !this.article.getCreateDate().contains(" ")) ? this.article.getCreateDate() : this.article.getCreateDate().split(" ")[0];
            textView.setText(createDate);
            this.contentView.setText(this.article.getContent());
            if (TextUtils.isEmpty(this.article.getNewsSource())) {
                this.newsSourceContainer.setVisibility(View.GONE);
            } else {
                this.newsSourceView.setVisibility(View.VISIBLE);
                this.newsSourceView.setText(this.article.getNewsSource());
                if (!TextUtils.isEmpty(this.article.getNewsDate())) {
                    textView = this.newsDateView;
                    if (this.article.getNewsDate().contains(" ")) {
                        createDate = this.article.getNewsDate().split(" ")[0];
                    } else {
                        createDate = this.article.getNewsDate();
                    }
                    textView.setText(createDate);
                }
                if (!TextUtils.isEmpty(this.article.getNewsLink())) {
                    this.newsSourceView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                }
            }
            if (TextUtils.isEmpty(this.article.getPhotoIds())) {
                this.photoContainer.setVisibility(View.GONE);
            }
        }
    }

    private List<Integer> getArticlePhotoIds(List<Photo> photos) {
        List<Integer> ids = new ArrayList();
        for (Photo photo : photos) {
            ids.add(Integer.valueOf(photo.getId()));
        }
        return ids;
    }

    public void setArticle(Article article) {
        this.article = article;
        setTitle(article.getTitle());
    }

    public void loadData() {
        if (!TextUtils.isEmpty(this.article.getPhotoIds()) && this.photos.isEmpty()) {
            List<Photo> photos = this.photoAlbumDao.getPhotosByArticleId(this.article.getId());
            if (photos == null || photos.size() <= 0) {
                String[] tmp = this.article.getPhotoIds().split(",");
                if (tmp.length > 0) {
                    for (String id : tmp) {
                        if (id.length() >= 1) {
                            id = id.trim();
                            Photo photo = new Photo();
                            photo.setId(Integer.parseInt(id));
                            this.photos.add(photo);
                        }
                    }
                    return;
                }
                return;
            }
            this.photos.clear();
            this.photos.addAll(photos);
        }
    }

    public void downloadArticle() {
        try {
            new ServerConnector().downloadLatestAritcleByCatid(this.article.getCategoryId(), new ServerDownloadCallback() {
                public void onDownloadFinished(Response response) {
                    Article[] articles = (Article[]) new Gson().fromJson(response.getResponseBody(), Article[].class);
                    if (articles != null && articles.length > 0) {
                        ArticleFragment.this.article.copy(articles[0]);
                    }
                }

                public void onDownloadFinishedUI(Response response) {
                    ArticleFragment.this.loadData();
                    ArticleFragment.this.init();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refresh(Photo photo, ImageView imageView) {
        imageView.setImageBitmap(ThumbnailUtils.extractThumbnail(Util.base64ToBitmap(photo.getContent()), 260, TransportMediator.KEYCODE_MEDIA_RECORD));
        ArticleFragment.this.leftBtn.bringToFront();
        ArticleFragment.this.rightBtn.bringToFront();
    }
}
