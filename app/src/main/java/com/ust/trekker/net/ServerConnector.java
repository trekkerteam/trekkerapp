package com.ust.trekker.net;

import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.Gson;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Response;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import uk.co.senab.photoview.IPhotoView;

public class ServerConnector extends AsyncTask<String, String, Response> {
    private static final String DEV_URL = "http://143.89.191.90/portal/";
    private static final String DOWNLOAD_ABOUT_US = "article/getAboutUs";
    private static final String DOWNLOAD_ALL_ALBUM = "PhotoAlbum/getAllAlbum";
    private static final String DOWNLOAD_ALL_ALBUM_COVER = "";
    private static final String DOWNLOAD_ALL_CATEGORY = "category/getAllCategory";
    private static final String DOWNLOAD_ALL_GAME_QUESTION = "GameZoneQuestion/getAllQuestionArticles";
    private static final String DOWNLOAD_ALL_QUESTION = "";
    private static final String DOWNLOAD_ARICLE_COVERS_BY_ID = "PhotoAlbum/getCoverByArticleIds";
    private static final String DOWNLOAD_ARTICLE_BY_ID_YEAR = "article/getArticlesByCategoryIdAndYear";
    private static final String DOWNLOAD_ARTICLE_BY_YEAR = "Article/getArticlesByYear";
    private static final String DOWNLOAD_CATEGORY_BY_PARENTCATID_AND_YEAR = "category/getCategoryByParentCatIdAndYear";
    private static final String DOWNLOAD_CATEGORY_BY_YEAR = "category/getCategoryByYear";
    private static final String DOWNLOAD_HARMONIC_LIFE_CATEGORY = "category/getSubCateByParentCatId";
    private static final String DOWNLOAD_LATEST_BY_CAT_ID = "article/lasteArticlesByCategory";
    private static final String DOWNLOAD_PHOTO_BY_ALBUM_ID = "PhotoAlbum/getPhotoByAlbumId";
    private static final String DOWNLOAD_PHOTO_BY_ARTICLE_ID = "PhotoAlbum/getPhotoByArticleId";
    private static final String DOWNLOAD_PHOTO_BY_PHOTO_ID = "PhotoAlbum/getPhotoById";
    private static final String DOWNLOAD_QUESTION_BY_ARTICLE_ID = "GameZoneQuestion/getQestionsByArticelId";
    private static final String DOWNLOAD_QUESTION_BY_GAME = "";
    private static final String DOWNLOAD_QUESTION_BY_ID = "";
    public static final String DOWNLOAD_THUMBNAIL_BY_ARTICLE_ID = "photoAlbum/getThumbnailByArticleId";
    public static final String DOWNLOAD_THUMBNAIL_BY_PHOTO_ID = "photoAlbum/getThumbnailByPhotoId";
    private static final String GET_DEELTED_ITEM = "user/findDeletedItem";
    private static final String LOGIN_USER = "User/mobileLogin";
    private static final String PRO_URL = "http://180.168.6.34:8091/portal/";
    private static final String SEND_STATIC = "statistic/record";
    private static final String TAG = "serverConnector";
    private static final String UAT_URL = "http://143.89.191.90/portal/";
    private static ConnectionQueue connectionns;
    private static String serverUrl;
    private ServerDownloadCallback callback;
    private boolean cancelled;
    private String postParam;
    private String url;

    public static abstract class ServerDownloadCallback {
        public abstract void onDownloadFinished(Response response);

        public void onDownloadFinishedUI(Response response) {
        }
    }

    public static class ConnectionQueue {
        private List<ServerConnector> connections;

        public ConnectionQueue() {
            this.connections = new ArrayList();
        }

        public void add(ServerConnector con) {
            this.connections.add(con);
        }

        public void remove(ServerConnector con) {
            this.connections.remove(con);
        }

        public void cancelAll() {
            for (ServerConnector con : this.connections) {
                con.cancel(true);
            }
        }
    }

    public ServerConnector() {
        this.cancelled = false;
    }

    static {
        connectionns = new ConnectionQueue();
        serverUrl = PRO_URL;
    }

    public static void setDev() {
        serverUrl = UAT_URL;
    }

    public static void setUat() {
        serverUrl = UAT_URL;
    }

    public static void setPro() {
        serverUrl = PRO_URL;
    }

    public static void cancelAll() {
        connectionns.cancelAll();
    }

    protected void execute() {
        super.execute(new String[0]);
        connectionns.add(this);
    }

    public void userLogin(String token, ServerDownloadCallback callback) throws UnsupportedEncodingException {
        this.url = serverUrl + LOGIN_USER;
        this.postParam = "token=" + URLEncoder.encode(token, App.URL_ENCODE_FORMAT);
        this.callback = callback;
        App.cred = token;
        execute();
    }

    public void downloadArticleByIdAndYear(int id, String year, ServerDownloadCallback callback) {
        this.url = serverUrl + DOWNLOAD_ARTICLE_BY_ID_YEAR + "?id=" + id + "&year=" + year;
        this.callback = callback;
        execute();
    }

    public void downloadCategoryByYear(String year, int lastId, ServerDownloadCallback callback) {
        this.url = serverUrl + DOWNLOAD_CATEGORY_BY_YEAR + "?year=" + year + "&lastId=" + lastId;
        this.callback = callback;
        execute();
    }

    public void getCategoryByParentCatIdAndYear(int parentCatId, String year, ServerDownloadCallback callback) {
        this.url = serverUrl + DOWNLOAD_CATEGORY_BY_PARENTCATID_AND_YEAR + "?parentCatId=" + parentCatId + "&year=" + year;
        this.callback = callback;
        execute();
    }

    public void downloadAboutUs(ServerDownloadCallback callback) {
        this.url = serverUrl + DOWNLOAD_ABOUT_US;
        this.callback = callback;
        execute();
    }

    public void downloadArticleByYear(String year, int lastId, ServerDownloadCallback callback) {
        this.url = serverUrl + DOWNLOAD_ARTICLE_BY_YEAR + "?year=" + year + "&lastId=" + lastId;
        this.callback = callback;
        execute();
    }

    public void getAllAlbum(int lastId, ServerDownloadCallback callback) {
        this.url = serverUrl + DOWNLOAD_ALL_ALBUM + "?lastId=" + lastId;
        this.callback = callback;
        execute();
    }

    public void downloadPhotoByPhotoId(String id, ServerDownloadCallback callback) {
        this.url = serverUrl + DOWNLOAD_PHOTO_BY_PHOTO_ID + "?photoId=" + id;
        this.callback = callback;
        execute();
    }

    public void getAllQuestions(ServerDownloadCallback callback) {
        this.url = serverUrl + DOWNLOAD_QUESTION_BY_ID;
        this.callback = callback;
        execute();
    }

    public void getQuestionById(String id, ServerDownloadCallback callback) throws UnsupportedEncodingException {
        this.url = serverUrl + DOWNLOAD_QUESTION_BY_ID + "?id=" + URLEncoder.encode(id, App.URL_ENCODE_FORMAT);
        this.callback = callback;
        execute();
    }

    public void downloadAllQuestionArticle(ServerDownloadCallback callback) {
        this.url = serverUrl + DOWNLOAD_ALL_GAME_QUESTION;
        this.callback = callback;
        execute();
    }

    public void downloadQuestionByArticleId(int articleId, ServerDownloadCallback callback) {
        this.url = serverUrl + DOWNLOAD_QUESTION_BY_ARTICLE_ID + "?articleId=" + articleId;
        this.callback = callback;
        execute();
    }

    public void downloadPhotoByArticleId(int articleId, ServerDownloadCallback callback) throws UnsupportedEncodingException {
        this.url = serverUrl + DOWNLOAD_PHOTO_BY_ARTICLE_ID + "?articleId=" + URLEncoder.encode(String.valueOf(articleId), App.URL_ENCODE_FORMAT);
        this.callback = callback;
        execute();
    }

    public void downloadLatestAritcleByCatid(int catId, ServerDownloadCallback callback) throws UnsupportedEncodingException {
        this.url = serverUrl + DOWNLOAD_LATEST_BY_CAT_ID + "?catId=" + URLEncoder.encode(String.valueOf(catId), App.URL_ENCODE_FORMAT);
        this.callback = callback;
        execute();
    }

    public void getDeletedItem(String tableName, String ids, ServerDownloadCallback callback) {
        this.url = serverUrl + GET_DEELTED_ITEM + "?tableName=" + tableName + "&ids=" + ids;
        this.callback = callback;
        execute();
    }

    public void sendStatic(String cred, String id) {
        this.url = serverUrl + SEND_STATIC + "?userCred=" + cred + "&catId=" + id + "&date=" + new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        execute();
    }

    public void query(String url, String[] kvps, ServerDownloadCallback callback) {
        this.url = serverUrl + url;
        String getParams = DOWNLOAD_QUESTION_BY_ID;
        if (kvps != null && kvps.length > 0) {
            getParams = getParams + "?";
            for (String kvp : kvps) {
                getParams = getParams + kvp + "&";
            }
            getParams = getParams.substring(0, getParams.length() - 1);
        }
        this.url += getParams;
        this.callback = callback;
        execute();
    }

    public void onCancelled() {
        this.cancelled = true;
    }

    protected Response doInBackground(String... strings) {
        if (this.cancelled) {
            return new Response();
        }
        Response response;
        try {
            response = (Response) new Gson().fromJson(queryServer(), Response.class);
        } catch (Exception e) {
            e.printStackTrace();
            response = new Response();
            response.ErrCode = App.E111;
            response.ErrMsg = "unknown error";
            response.ResponseBody = "error";
        }
        if (this.callback == null) {
            return response;
        }
        this.callback.onDownloadFinished(response);
        return response;
    }

    protected void onPostExecute(Response response) {
        connectionns.remove(this);
        if (response != null && this.callback != null && !this.cancelled) {
            this.callback.onDownloadFinishedUI(response);
        }
    }

    private String queryServer() throws IOException {
        HttpURLConnection con = (HttpURLConnection) new URL(this.url).openConnection();
        con.setReadTimeout(10000);
        con.setConnectTimeout(10000);
        con.setDoInput(true);
        if (this.postParam != null) {
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, App.URL_ENCODE_FORMAT));
            Log.d(TAG, this.postParam);
            writer.write(this.postParam);
            writer.flush();
            writer.close();
            os.close();
        } else {
            con.setRequestMethod("GET");
        }
        con.connect();
        int responseCode = con.getResponseCode();
        Log.d(TAG, "responseCode: " + responseCode + ", responseMSG: " + con.getResponseMessage());
        if (responseCode == IPhotoView.DEFAULT_ZOOM_DURATION) {
            return inputStreamToString(con.getInputStream());
        }
        return null;
    }

    private String inputStreamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        while (true) {
            String line = reader.readLine();
            if (line == null) {
                return total.toString();
            }
            total.append(line);
        }
    }
}
