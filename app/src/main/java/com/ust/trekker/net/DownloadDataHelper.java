package com.ust.trekker.net;

import com.google.gson.Gson;
import com.ust.trekker.Dao.ArticleDao;
import com.ust.trekker.Dao.CategoryDao;
import com.ust.trekker.constant.App;
import com.ust.trekker.model.Article;
import com.ust.trekker.model.Category;
import com.ust.trekker.model.Response;
import com.ust.trekker.net.ServerConnector.ServerDownloadCallback;
import uk.co.senab.photoview.BuildConfig;

public class DownloadDataHelper {

    public interface Callback {
        void onDownloadFinished();
    }

    public static void updateCategory(final CategoryDao categoryDao, final Callback callback) {
        final int lastId = categoryDao.getLastUpdateId();
        new ServerConnector().downloadCategoryByYear(BuildConfig.FLAVOR, lastId, new ServerDownloadCallback(){

            public void onDownloadFinished(Response response) {
                if (response.ErrCode.equals(App.E000)) {
                    Category[] categories = (Category[]) new Gson().fromJson(response.ResponseBody, Category[].class);
                    categoryDao.save(categories);
                    int max = lastId;
                    for (Category c : categories) {
                        if (c.getId() > max) {
                            max = c.getId();
                        }
                    }
                    if (lastId < max) {
                        categoryDao.updateLastId(max);
                    }
                }
            }

            public void onDownloadFinishedUI(Response response) {
                callback.onDownloadFinished();
            }
        });
    }

    public static void updateArticle(final ArticleDao articleDao, final Callback callback) {
        final int lastId = articleDao.getLastUpdateId();
        new ServerConnector().downloadArticleByYear("", lastId, new ServerDownloadCallback() {
            public void onDownloadFinished(Response response) {
                if (response.ErrCode.equals(App.E000)) {
                    Article[] articles = (Article[]) new Gson().fromJson(response.ResponseBody, Article[].class);
                    articleDao.save(articles);
                    int max = lastId;
                    for (Article a : articles) {
                        if (a.getId() > max) {
                            max = a.getId();
                        }
                    }
                    if (lastId < max) {
                        articleDao.updateLastId(max);
                    }
                }
            }

            public void onDownloadFinishedUI(Response response) {
                callback.onDownloadFinished();
            }
        });
    }
}
