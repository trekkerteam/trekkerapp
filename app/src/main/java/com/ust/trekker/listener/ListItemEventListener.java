package com.ust.trekker.listener;

public interface ListItemEventListener {
    void onItemSelected(Object obj);
}
