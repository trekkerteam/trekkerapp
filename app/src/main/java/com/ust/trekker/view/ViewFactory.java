package com.ust.trekker.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import com.ust.trekker.R;

public class ViewFactory {

    /* renamed from: com.ust.trekker.view.ViewFactory.1 */
    static class C06361 implements OnClickListener {
        C06361() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            dialog.dismiss();
        }
    }

    /* renamed from: com.ust.trekker.view.ViewFactory.2 */
    static class C06372 implements OnClickListener {
        final /* synthetic */ Activity val$activity;

        C06372(Activity activity) {
            this.val$activity = activity;
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            ViewFactory.callChrome(this.val$activity, "http://180.168.6.34:8091/apk/trekkerv1_0_3.apk");
        }
    }

    public static AlertDialog warnningMsg(Context context, String msg) {
        Builder builder = new Builder(context);
        builder.setTitle(context.getString(R.string.system_msg_label));
        builder.setMessage(msg);
        builder.setPositiveButton(context.getString(R.string.confirm_label), new C06361());
        return builder.create();
    }

    public static AlertDialog versionWarnning(Context context, String msg, Activity activity) {
        Builder builder = new Builder(context);
        builder.setTitle(context.getString(R.string.system_msg_label));
        builder.setMessage(msg);
        builder.setPositiveButton(context.getString(R.string.confirm_label), new C06372(activity));
        return builder.create();
    }

    private static void callChrome(Activity activity, String url) {
        Intent i = new Intent("android.intent.action.VIEW", Uri.parse(url));
        i.addFlags(268435456);
        i.setPackage("com.android.chrome");
        try {
            activity.startActivity(i);
        } catch (ActivityNotFoundException e) {
            i.setPackage(null);
            activity.startActivity(i);
        }
    }

    private static void exit(Activity activity) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        intent.setFlags(268468224);
        activity.startActivity(intent);
    }
}
