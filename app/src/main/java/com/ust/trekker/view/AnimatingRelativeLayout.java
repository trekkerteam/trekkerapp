package com.ust.trekker.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import com.ust.trekker.R;

public class AnimatingRelativeLayout extends RelativeLayout {
    Context context;
    Animation inAnimation;
    Animation outAnimation;

    public AnimatingRelativeLayout(Context context) {
        super(context);
        this.context = context;
        initAnimations();
    }

    public AnimatingRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initAnimations();
    }

    public AnimatingRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        initAnimations();
    }

    private void initAnimations() {
        //this.inAnimation = AnimationUtils.loadAnimation(this.context, R.anim.in_slide);
        //this.outAnimation = AnimationUtils.loadAnimation(this.context, R.anim.out_slide);
    }

    public void show() {
        if (!isVisible()) {
            show(true);
        }
    }

    public void show(boolean withAnimation) {
        if (withAnimation) {
            startAnimation(this.inAnimation);
        }
        setVisibility(VISIBLE);
    }

    public void hide() {
        if (isVisible()) {
            hide(true);
        }
    }

    public void hide(boolean withAnimation) {
        if (withAnimation) {
            startAnimation(this.outAnimation);
        }
        setVisibility(INVISIBLE);
    }

    public boolean isVisible() {
        return getVisibility() == GONE;
    }

    public void overrideDefaultInAnimation(Animation inAnimation) {
        this.inAnimation = inAnimation;
    }

    public void overrideDefaultOutAnimation(Animation outAnimation) {
        this.outAnimation = outAnimation;
    }
}
